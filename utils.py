import plotly
import plotly.graph_objs as go
import plotly.express as px
import json
import pandas as pd
from plotly.subplots import make_subplots
from collections import defaultdict
from flowmaps.flowmaps import FlowMaps
from flowmaps.constants import province_id_to_name, province_to_ccaa
from datetime import datetime, timedelta
from sklearn.linear_model import LinearRegression

from config import mongodb_params, province_centroids



def encode_figure(fig):
    data = json.dumps(fig.data, cls=plotly.utils.PlotlyJSONEncoder)
    layout = json.dumps(fig.layout, cls=plotly.utils.PlotlyJSONEncoder)
    return data, layout


def read_provinces_geojson():
    with open('static/data/cnig_provincias.geojson') as json_file:
        geojson = json.load(json_file)
        return geojson


def read_hires_geojson():
    with open('static/data/hires.geojson') as json_file:
        geojson = json.load(json_file)
        return geojson


def get_available_date_range_for_risk():
    # return '2020-04-14' # TODO!!!
    fm = FlowMaps(mongodb_params)
    # The following should be fast, if there is an index in 'date', and
    # note that it will not compute the full sort()
    # https://stackoverflow.com/q/32076382

    cursor = fm.db['mitma_mov.daily_mobility_matrix'].find({'source_layer': 'cnig_provincias', 'target_layer': 'cnig_provincias'}, {'date': -1, '_id':0}).sort('date', -1).limit(1)
    mobility_max_date = next(cursor)['date']
    cursor = fm.db['mitma_mov.daily_mobility_matrix'].find({'source_layer': 'cnig_provincias', 'target_layer': 'cnig_provincias'}, {'date': -1, '_id':0}).sort('date', 1).limit(1)
    mobility_min_date = next(cursor)['date']
    print(f'mobility_min_date: {mobility_min_date}, mobility_max_date: {mobility_max_date}')

    cursor = fm.db['layers.data.consolidated'].find({'type': 'covid19', 'ev': 'ES.covid_cpro'}, {'date': -1, '_id':0}).sort('date', -1).limit(1)
    covid_max_date = next(cursor)['date']
    cursor = fm.db['layers.data.consolidated'].find({'type': 'covid19', 'ev': 'ES.covid_cpro'}, {'date': -1, '_id':0}).sort('date', 1).limit(1)
    covid_min_date = next(cursor)['date']
    print(f'covid_min_date: {covid_min_date}, covid_max_date: {covid_max_date}')

    cursor = fm.db['layers.data.consolidated'].find({'type': 'population', 'layer': 'cnig_provincias'}, {'date': -1, '_id':0}).sort('date', -1).limit(1)
    pop_max_date = next(cursor)['date']
    cursor = fm.db['layers.data.consolidated'].find({'type': 'population', 'layer': 'cnig_provincias'}, {'date': -1, '_id':0}).sort('date', 1).limit(1)
    pop_min_date = next(cursor)['date']
    print(f'pop_min_date: {covid_min_date}, pop_max_date: {covid_max_date}')

    # ## TODO: THIS IS A QUICK FIX
    # if max_date > '2020-09-10':
    #     max_date = '2020-09-10'
    # ############

    min_date = max(mobility_min_date, covid_min_date, pop_min_date)
    max_date = min(mobility_max_date, covid_max_date, pop_max_date)
    return min_date, max_date


def get_available_date_range_for_incidence():
    # return '2020-04-14' # TODO!!!
    fm = FlowMaps(mongodb_params)
    # The following should be fast, if there is an index in 'date', and
    # note that it will not compute the full sort()
    # https://stackoverflow.com/q/32076382

    cursor = fm.db['layers.data.consolidated'].find({'type': 'covid19', 'ev': 'ES.covid_cpro'}, {'date': -1, '_id':0}).sort('date', -1).limit(1)
    covid_max_date = next(cursor)['date']
    # cursor = fm.db['layers.data.consolidated'].find({'type': 'covid19', 'ev': 'ES.covid_cpro'}, {'date': -1, '_id':0}).sort('date', 1).limit(1)
    # covid_min_date = next(cursor)['date']
    covid_min_date = '2020-03-01'  # First relevant date in terms of covid cases in Spain 2020
    print(f'covid_min_date: {covid_min_date}, covid_max_date: {covid_max_date}')

    # ## TODO: THIS IS A QUICK FIX
    # if max_date > '2020-09-10':
    #     max_date = '2020-09-10'
    # ############

    min_date = covid_min_date
    max_date = covid_max_date
    return min_date, max_date


def read_detailed_risk_df(province_id, date_str):
    fm = FlowMaps(mongodb_params)
    doc = fm.query('FlowBoard.cache_risk',
                  layer='cnig_provincias',
                  id=province_id,
                  date=date_str).get_first()
    df = pd.DataFrame(doc['detailed_risk'])

    # compute active_cases_per_100000
    df['active_per_100000'] = (df['active_cases']/df['population']).apply(lambda x: round(x*100000, 2))

    # round values
    df['risk'] = df['risk'].apply(lambda x: round(x, 3))
    df['trips'] = df['trips'].apply(lambda x: int(x))
    df['population'] = df['population'].apply(lambda x: int(x))

    # get province names
    df['province_name'] = df['source'].apply(province_id_to_name.get)

    # get risk tendency
    tendency = get_risk_tendency(province_id, date_str)
    def add_sign(x):
        if x >= 0:
            return '+'+str(x)
        else:
            return str(x)
    df['risk_tendency'] = df['source'].apply(lambda x: add_sign(round(tendency.get(x, 0), 2)))

    return df.sort_values('risk', ascending=False).reset_index()


def get_info_box(province_id, date_str):
    fm = FlowMaps(mongodb_params)
    data = fm.query('FlowBoard.cache_risk',
                  layer='cnig_provincias',
                  id=province_id,
                  date=date_str).get_first()
    fields = ['incoming_risk', 'id', 'incoming_trips', 'total_cases', 'area', 'max_cases', 'max_cases_date', 'total_incidence', 'total_cases_per_100000', 'pop_density', 'population']
    return {k:data[k] for k in data if k in fields}


def create_top_plot(province_id, date_str):
    fm = FlowMaps(mongodb_params)
    data = fm.query('FlowBoard.cache_risk', layer='cnig_provincias', id=province_id, date={'$lte': date_str}).project({'detailed_risk': 1, 'date':1}).get().to_list()
    rows = []
    for result in data:
        date = result['date']
        for item in result['detailed_risk']:
            rows.append({'date': date, 'risk': item['risk'], 'source': item['source']})
    df = pd.DataFrame(rows)

    fig = go.Figure()

    for source_id in df[df['date'] == df['date'].max()]['source'].to_list():
        subdf = df[df['source'] == source_id]
        fig.add_trace(go.Scatter(x=subdf['date'], y=subdf['risk'],
                        mode='lines',
                        hovertemplate="%{x|%Y-%m-%d (%A)}<br>Incoming risk: %{y}",
                        name=province_id_to_name[source_id]))

    fig.update_layout(margin={"r":15,"t":0,"l":0,"b":0}, height=300)
    return fig


def create_risk_lineplot(province_id):
    fm = FlowMaps(mongodb_params)
    df = fm.query('FlowBoard.cache_risk',
                  layer='cnig_provincias',
                  id=province_id) \
           .project({'detailed_risk': 0}) \
           .get().to_df().sort_values('date')

    # apply a windowed mean
    df = df.set_index('date').rolling(1, center=False, min_periods=0)[['new_cases', 'incoming_risk']].mean().reset_index()

    df['date'] =  pd.to_datetime(df['date'], format='%Y-%m-%d')
    df['incoming_risk'] = df['incoming_risk'].apply(lambda x: round(x, 2))
    df['new_cases'] = df['new_cases'].apply(lambda x: round(x, 0))

    # plot risk
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.add_trace(go.Scatter(x=df['date'], y=df['incoming_risk'],
                        mode='lines',
                        name='Incoming risk',
                        hovertemplate="%{x|%Y-%m-%d (%A)}<br>Incoming risk: %{y}",
                        line=dict(color='red')))

    # plot cases
    fig.add_trace(go.Scatter(x=df['date'], y=df['new_cases'],
                        mode='lines',
                        name='Daily cases',
                        line=dict(width=1, color='blue'),
                        hovertemplate="%{x|%Y-%m-%d (%A)}<br>Daily cases: %{y}",
                        visible=True), secondary_y=True)

    # Set y-axes titles
    fig.update_yaxes(title_text="Risk", secondary_y=False)
    fig.update_yaxes(title_text="Daily cases", secondary_y=True)


    # fig.update_layout(clickmode='event+select')
    fig.update_layout(
        autosize=True,
        height=300,
        # width=600,
        margin_r=0,
        margin_l=0,
        margin_t=80,
        margin_b=20,
        # yaxis_title="",
        yaxis_fixedrange=True,
        # dragmode=False,
        # dragmode='select',
        # selectdirection='h',
        # autosize=True,
        # showlegend=False,
    )
    fig.update_layout(legend={'orientation': 'h', 'yanchor': 'bottom', 'xanchor': 'right', 'y': 1.02, 'x': 1})
    fig.update_traces(marker_size=20)
    return fig


def get_provinces_status(date_str):
    fm = FlowMaps(mongodb_params)
    df = fm.query('FlowBoard.cache_risk').filter({'layer': 'cnig_provincias', 'date': date_str}).project({'detailed_risk': 0}).get().to_df()
    df['name'] = df['id'].apply(province_id_to_name.get)
    df['new_cases'] = df['new_cases'].apply(lambda x: round(x, 0))
    df['incoming_trips'] = df['incoming_trips'].apply(lambda x: round(x, 0))
    df['incoming_risk'] = df['incoming_risk'].apply(lambda x: round(x, 2))
    df['population'] = df['population'].apply(lambda x: round(x, 0))
    df['cases_per_100000'] = (df['new_cases']/df['population']).apply(lambda x: round(x*100000, 0))
    rename = {
        'new_cases': 'Daily cases',
        'cases_per_100000': 'Daily cases per 100000 inhabitants',
        'name': 'Province',
        'incoming_trips': 'Incoming trips',
        'incoming_risk': 'Incoming risk',
        'population': 'Population',
        'id': 'Id',
    }
    df = df.rename(columns=rename)
    df = df[rename.values()]
    return df


def create_home_provinces_map(date_str):
    geojson = read_provinces_geojson()
    df = get_provinces_status(date_str)
    lon, lat = province_centroids['28']
    fig = px.choropleth_mapbox(data_frame=df,
                                geojson=geojson,
                                locations='Id',
                                color='Daily cases per 100000 inhabitants',
                                color_continuous_scale="Reds",
                                # range_color=(0, 1),
                                hover_data=['Province', 'Daily cases per 100000 inhabitants', 'Daily cases', 'Incoming trips', 'Incoming risk', 'Population'],
                                mapbox_style="carto-positron",
                                opacity=0.5,
                                zoom=5,
                                center={"lat":lat, "lon":lon})
    fig.update_layout(mapbox_style="carto-positron",
                      # clickmode='event+select',
                      height=500)
    fig.update_layout(margin={"r":15,"t":0,"l":0,"b":0})
    return fig


def create_risk_map(province_id, df):
    # keep_columns = ['source_population', 'province', 'population', 'active_cases', 'active_per_100000', 'trips', 'risk']
    # df = df[keep_columns]
    rename = {
        'source': 'Id',
        'risk': 'Risk',
        'province_name': 'Province',
        'trips': 'Trips',
        'population': 'Population',
        'active_cases': 'Active cases',
        'active_per_100000': 'Active cases per 100000 inhabitants',
    }
    df = df.rename(columns=rename)
    geojson = read_provinces_geojson()
    
    lon, lat = province_centroids[province_id]
    fig = px.choropleth_mapbox(data_frame=df,
                                geojson=geojson,
                                locations='Id',
                                color='Risk',
                                color_continuous_scale="Reds",
                                # range_color=(0, 1),
                                hover_data=['Province', 'Population', 'Active cases', 'Active cases per 100000 inhabitants', 'Trips'],
                                mapbox_style="carto-positron",
                                opacity=0.5,
                                zoom=5,
                                center={"lat":lat, "lon":lon})
    fig.update_layout(mapbox_style="carto-positron",
                      # clickmode='event+select',
                      height=500)
    fig.update_layout(
        autosize=True,
        # height=300,
        # width=600,
        margin_r=0,
        margin_l=0,
        margin_t=30,
        margin_b=0,
    )

    province_geojson = {
        'type': 'FeatureCollection',
        'features': [g for g in geojson['features'] if g['id'] == province_id]
    }
    mapbox_layers = [
        { 'sourcetype': 'geojson', 'source': province_geojson,
          'below': '', 'type': 'line', 
          'color': 'black', 'opacity': 1, 
          'line': {'width': 2.5}
        }
    ]
    
    fig.layout.update(mapbox_layers=mapbox_layers)
    return fig


def create_local_risk_map(province_id):
    ccaa_id = province_to_ccaa[province_id]
    geojson = read_hires_geojson()
    local_geojson = {
        'type': geojson['type'],
        'features': [f for f in geojson['features'] if f['ccaa_id'] == ccaa_id]
    }
    # print(f"local_geojson has {len(local_geojson['features'])} polygons")
    df = pd.DataFrame({'Id': f['id'], 'Risk': 1} for f in local_geojson['features'])
    lon, lat = province_centroids[province_id]
    fig = px.choropleth_mapbox(data_frame=df,
                                geojson=local_geojson,
                                locations='Id',
                                color='Risk',
                                color_continuous_scale="Reds",
                                # range_color=(0, 1),
                                hover_data=[],
                                mapbox_style="carto-positron",
                                opacity=0.5,
                                zoom=6,
                                center={"lat":lat, "lon":lon})
    fig.update_layout(mapbox_style="carto-positron",
                      clickmode='event',
                      height=500)
    fig.update_layout(margin={"r":15,"t":0,"l":0,"b":0})
    return fig


def risk_table(province_id, df):
    keep_columns = ['province_name', 'risk', 'population', 'active_cases', 'active_per_100000', 'trips', 'risk_tendency']
    df = df[keep_columns]
    rename = {
        'risk': 'Risk (R = V * C/T)',
        'province_name': 'Province',
        'trips': 'Trips from origin (V)',
        'population': 'Total origin population (T)',
        'active_cases': 'Active Covid-19 cases at origin (C)',
        'active_per_100000': 'Active Covid-19 cases per 100.000 inhabitants',
        'risk_tendency': 'Risk tendency (last 7 days)',
    }
    df = df.rename(columns=rename)
    columns = [rename[k] for k in keep_columns]
    fig = go.Figure(data=[go.Table(
        header=dict(values=[f"<b>{k}</b>" for k in columns],
                    fill_color='#f0f0f0',
                    font_size=15,
                    align='left'),
        cells=dict(values=[df[k] for k in columns],
                   fill_color='#fafafa',
                   font_size=15,
                   align='left',
                   height=30))
    ])
    fig.update_layout(margin={"r":15,"t":0,"l":0,"b":0})
    return fig


def get_risk_tendency(province_id, date_str):
    # TODO: cache this and store in mongodb
    # issue: this needs the risk for the last 7 days to be available in the db
    fm = FlowMaps(mongodb_params)
    end_date = datetime.strptime(date_str, '%Y-%m-%d')
    start_date = end_date-timedelta(days=7)
    end_date_str = end_date.strftime('%Y-%m-%d')
    start_date_str = start_date.strftime('%Y-%m-%d')
    query = fm.query("FlowBoard.cache_risk", layer='cnig_provincias', id=province_id, date={'$gte': start_date_str, '$lte': end_date_str}).project({'detailed_risk': 1, 'date': 1})
    risk_by_date = query.get().to_dict(key='date', value='detailed_risk')
    risk_by_source = defaultdict(list)
    for date, sourcelist in sorted(risk_by_date.items(), key=lambda x: x[0], reverse=False):
        for risk in sourcelist:
            risk_by_source[risk['source']].append((date, risk['risk']))
    tendencies = {}
    for source, risk_history in risk_by_source.items():
        X = [[x] for x in range(len(risk_history))]
        y = [x[1] for x in risk_history]
        # plt.plot(y)
        reg = LinearRegression().fit(X, y)
        tendencies[source] = reg.coef_[0]
    return tendencies


def create_spain_cases_plot(date_str):
    fm = FlowMaps(mongodb_params)
    df = fm.data(ev='ES.covid_cca').add_date_field().flatten_fields({'d.num_casos': 'cases'}).group_by(['date'], ['cases'], count=True).sort('date').get().to_df()
    df = df[df['date']<=date_str]
    df['date'] =  pd.to_datetime(df['date'], format='%Y-%m-%d')
    
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df['date'], y=df['cases'],
                        mode='lines',
                        name='Daily cases',
                        hovertemplate="%{x|%Y-%m-%d (%A)}<br>Daily cases: %{y}",
                        # line=dict(width=1, color='blue'),
                        )
    )

    # Set y-axes titles
    fig.update_yaxes(title_text="Daily cases")
    fig.update_layout(
        margin={"r":55,"t":0,"l":0,"b":0},
        height=300,
        legend={'orientation': 'h', 'yanchor': 'bottom', 'xanchor': 'right', 'y': 1.02, 'x': 1},
        showlegend=False,
        # plot_bgcolor="#f5f5f5",
    )
    return fig
