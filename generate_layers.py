import sys
import os

sys.path.append('../../')

from flowmaps.flowmaps import FlowMaps
from flowmaps.geojson import FeatureCollection
from config import mongodb_params
from flowmaps.constants import ccaa_to_provinces, ccaa_data_sources, layers_name_fields

fm = FlowMaps(mongodb_params)

target_folder = './static/data'
if not os.path.isdir(target_folder):
    os.mkdir(target_folder)


def download_layer(layer_id, name=None, simplify=False):
    if name is None:
        name = layer_id
    print(f'downloading layer {layer_id}')
    geojson = fm.layer(layer_id).get().to_geojson()
    name_field = layers_name_fields.get(layer_id)

    for feat in geojson['features']:
        feat['_id'] = feat['id']
        feat['_layer'] = layer_id
        feat['name'] = feat['properties'].get(name_field)
        # feat['id'] = layer_id+':'+feat['id']  # append layer name so that ids do not collide
    geojson.save(f'{target_folder}/{name}.geojson')
    if simplify:
        geojson = geojson.simplify_polygons(ratio=0.1, min_points=10)
    return geojson


def download_province_sublayer(layer_id, province_id):
    province_polygon = fm.layer('cnig_provincias', id=province_id).get_first()
    geojson = province_polygon.query_overlapping_polygons(target_layer=layer_id).get().to_geojson()
    name_field = layers_name_fields.get(layer_id)
    for feat in geojson['features']:
        feat['_id'] = feat['id']
        feat['_layer'] = layer_id
        feat['name'] = feat['properties'].get(name_field)
    geojson = geojson.simplify_polygons(ratio=0.1, min_points=10)
    return geojson


cnig_ccaa = download_layer('cnig_ccaa')
cnig_provincias = download_layer('cnig_provincias')

######
# For several CCAA we have high resolution data (for a high resolution layer).
# For the CCAA whithout high resolution data, just download its provinces polygons.
# For the CCAA with high resolution data, download the high resolution layer, and split
# it for each province in the CCAA.
# We end up with all the provinces polygons except for those provinces where we have
# high resolution data.
######
for ccaa_id, data_sources in ccaa_data_sources.items():
    if data_sources.get('incidence'):
        # We have high resolution data for this CCAA
        # Download all provinces its provinces
        print(f"\nCCAA {ccaa_id} -> a high resolution layer is available, downloading high resolution polygons for provinces: "+str(ccaa_to_provinces[ccaa_id]))
        for province_id in ccaa_to_provinces[ccaa_id]:
            print('  - downloading high resolution layer for province: '+province_id)
            layer_id = data_sources.get('incidence')[0]['layer']
            geojson = download_province_sublayer(layer_id, province_id)
            name = 'hires_province_'+province_id
            geojson.save(f'{target_folder}/{name}.geojson')

    else:
        print(f"\nCCAA {ccaa_id} -> no sublayer found, skipping")
        # ccaa_provinces_ids = ccaa_to_provinces[ccaa_id]
        # print(f"\nCCAA {ccaa_id} -> no sublayer for ccaa {ccaa_id}, downloading polygons of provinces: {ccaa_provinces_ids}")
        # features = [f for f in cnig_provincias['features'] if f['id'] in ccaa_provinces_ids]
        # geojson = FeatureCollection(features=features)
        # name = 'hires_'+ccaa_id
        # geojson.save(f'{target_folder}/{name}.geojson')

# features = []

# for ccaa_id, data_sources in ccaa_data_sources.items():
#   if data_sources.get('incidence'):
#       sublayer_id = data_sources['incidence'][0]['layer']
#       ccaa_polygon = fm.layer('cnig_ccaa', id=ccaa_id).get_first()
#       sublayer = ccaa_polygon.query_overlapping_polygons(target_layer=sublayer_id, use_centroid=True).get()
#       sublayer_geojson = sublayer.to_geojson()
#       print(f"adding {len(sublayer_geojson['features'])} polygons ({sublayer_id}) as sublayer of ccaa: {ccaa_id}")
#       for feat in sublayer_geojson['features']:
#           feat['_id'] = feat['id']
#           feat['_layer'] = sublayer_id
#           feat['id'] = sublayer_id+':'+feat['id']  # append layer name so that ids do not collide
#           feat['ccaa_id'] = ccaa_id
#       features += sublayer_geojson['features']
#   else:
#       subprovinces_ids = ccaa_to_provinces[ccaa_id]
#       print(f"adding provinces {subprovinces_ids} as sublayer of ccaa: {ccaa_id}")
#       provinces = fm.layer('cnig_provincias', id={'$in': subprovinces_ids}).get()
#       provinces_geojson = provinces.to_geojson()
#       for feat in provinces_geojson['features']:
#           feat['_id'] = feat['id']
#           feat['_layer'] = 'cnig_provincias'
#           feat['id'] = 'cnig_provincias'+':'+feat['id']  # append layer name so that ids do not collide
#           feat['ccaa_id'] = ccaa_id
#       features += provinces_geojson['features']

# sublayer = FeatureCollection(features=features)

# print(f"simplifying {len(sublayer['features'])} polygons")
# sublayer = sublayer.simplify_polygons(ratio=0.2, min_points=20)

# print(f"saving high resolution sublayer with {len(sublayer['features'])} polygons")
# sublayer.save(f'{target_folder}/hires_mixed.geojson')
