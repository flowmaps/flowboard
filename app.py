import sys
  
sys.path.append('../pyflowmaps/')

import pandas as pd
from pymongo.errors import ServerSelectionTimeoutError
import functools
from flask import Flask, render_template, request, url_for, flash, redirect, send_from_directory
from werkzeug.exceptions import abort
from flowmaps.flowmaps import FlowMaps
from flowmaps.constants import province_id_to_name, province_to_ccaa, ccaa_data_sublayer, layer_to_ev

from config import mongodb_params, province_centroids
import utils

app = Flask(__name__, static_url_path='/static')
app.config["APPLICATION_ROOT"] = "/flowboard"


def handle_timeout(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except ServerSelectionTimeoutError:
            return render_template("home-failure.html")

    return wrapper


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', description=e.description), 404


@app.route('/failure')
def failure():
    return render_template("home-failure.html")


@app.route('/static/<path:path>')
def send_asset(path):
    return send_from_directory('png', path)


@app.route('/', defaults={'date_str': None})
@app.route('/<date_str>')
@handle_timeout
def index(date_str=None):
    min_date, max_date = utils.get_available_date_range_for_risk()
    if date_str is None:
        date_str = max_date
    return render_template(
        'home.html', date_str=date_str,
        min_date=min_date, max_date=max_date,
    )


@app.route('/board_mobility/', defaults={'date_str': None})
@app.route('/board_mobility/<date_str>')
@handle_timeout
def board_mobility(date_str=None):
    min_date, max_date = utils.get_available_date_range_for_risk()
    if date_str is None:
        date_str = max_date
    return render_template(
        'board_mobility.html', date_str=date_str,
        min_date=min_date, max_date=max_date,
    )


@app.route('/board_what_is_risk')
@handle_timeout
def board_what_is_risk():
    return render_template('board_what_is_risk.html')


@app.route('/board_incidence/', defaults={'date_str': None})
@app.route('/board_incidence/<date_str>')
@handle_timeout
def board_incidence(date_str=None):
    min_date, max_date = utils.get_available_date_range_for_incidence()
    if date_str is None:
        date_str = max_date
    return render_template('board_incidence.html', date_str=date_str, min_date=min_date, max_date=max_date)


@app.route('/board_risk/', defaults={'date_str': None})
@app.route('/board_risk/<date_str>')
@handle_timeout
def board_risk(date_str=None):
    min_date, max_date = utils.get_available_date_range_for_risk()
    if date_str is None:
        date_str = max_date
    elif date_str > max_date:
        date_str = max_date
    elif date_str < min_date:
        date_str = min_date

    return render_template('board_risk.html',
                           date_str=date_str,
                           min_date=min_date,
                           max_date=max_date)


@app.route('/data_visualizer')
@handle_timeout
def data_visualizer():
    return render_template('data_visualizer.html')


@app.route('/data')
@handle_timeout
def data():
    return render_template('data.html')


@app.route('/contact')
@handle_timeout
def contact():
    return render_template('contact.html')


@app.route('/about')
@handle_timeout
def about():
    return render_template('about.html')

# @app.route('/data/risk/<area_id>/<date_str>')
# def data_risk(area_id, date_str):
#     layer_id, area_id = area_id.split(':')
#     fm = FlowMaps(mongodb_params)
#     try:
#         data = fm.query('FlowBoard.cache_risk', layer=layer_id, source_layer=layer_id, date=date_str, id=area_id).get_first()
#         data = {d['source']: d for d in data['detailed_risk']}
#     except StopIteration as e:
#         return {'error': str(e)}
#     return data


# @app.route('/data/risk/<area_id>/<date_str>')
# def data_risk(area_id, date_str):
#     layer_id, area_id = area_id.split(':')
#     fm = FlowMaps(mongodb_params)
#     try:
#         data = fm.query('FlowBoard.cache_risk', layer=layer_id, source_layer=layer_id, date=date_str, id=area_id).get_first()
#         data = {d['source']: d for d in data['detailed_risk']}
#     except StopIteration as e:
#         return {'error': str(e)}
#     return data


if __name__ == '__main__':
    app.run(debug=True)
