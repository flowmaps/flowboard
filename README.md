# FlowBoard (dashboard)

Link: https://flowmaps.life.bsc.es/dashboard/

The dashboard is built using [Flask](https://flask.palletsprojects.com/en/1.1.x/), [plotly](https://plotly.com/javascript/), and [Bootstrap](https://getbootstrap.com/).

It makes use of the REST Api to fetch the required data. 

TODO: currently (2020-10-03) it also uses pyflowmaps to load some of the data needed to build the tables and plots, but I am migrating it to REST API + Ajax for better performance

## Installation

There is a **install.sh** script in the root folder:

	bash install.sh

After the installation finishes, run the dashboard in debug mode:

	source env/bin/activate
	python app.py

To deploy on apache, copy the **dashboard.wsgi** file in ~/DOCUMENTS_ROOT, and add the following lines to the apache config:

	WSGIDaemonProcess dashboard python-home=/usr/local/venvs/empty user=user group=user threads=4 socket-user=user
    WSGIScriptAlias /dashboard /home/user/DOCUMENT_ROOT/dashboard.wsgi process-group=dashboard application-group=%{GLOBAL}

You may need to edit the ROOT_FOLDER location in **dashboard.wsgi**:

	ROOT_FOLDER = "/home/user/DASH_APPS/flowboard"  # place here the correct path to the installation

