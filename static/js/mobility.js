let leftMapId = 'leftMap'
let rightMapId = 'rightMap'
const province_id_to_name = {'48': 'Bizkaia', '13': 'Ciudad Real', '26': 'La Rioja', '22': 'Huesca', '20': 'Gipuzkoa', '46': 'València/Valencia', '15': 'A Coruña', '18': 'Granada', '31': 'Navarra', '33': 'Asturias', '34': 'Palencia', '51': 'Ceuta', '29': 'Málaga', '06': 'Badajoz', '07': 'Illes Balears', '49': 'Zamora', '45': 'Toledo', '16': 'Cuenca', '17': 'Girona', '10': 'Cáceres', '11': 'Cádiz', '09': 'Burgos', '42': 'Soria', '43': 'Tarragona', '21': 'Huelva', '28': 'Madrid', '08': 'Barcelona', '52': 'Melilla', '03': 'Alacant/Alicante', '12': 'Castelló/Castellón', '23': 'Jaén', '27': 'Lugo', '05': 'Ávila', '47': 'Valladolid', '04': 'Almería', '25': 'Lleida', '02': 'Albacete', '32': 'Ourense', '36': 'Pontevedra', '39': 'Cantabria', '44': 'Teruel', '50': 'Zaragoza', '35': 'Las Palmas', '19': 'Guadalajara', '40': 'Segovia', '24': 'León', '14': 'Córdoba', '01': 'Araba/Álava', '30': 'Murcia', '37': 'Salamanca', '38': 'Santa Cruz de Tenerife', '41': 'Sevilla'}
var plotly_options = {displaylogo: false, showlegend: false, displayModeBar: false, modeBarButtonsToRemove: ['toggleHover', 'hoverClosestGeo', 'pan2d', 'select2d', 'lasso2d']}
var ply_config = { displayModeBar: false }
var selected_area_id = '08';
let colorMobility = '#001583' // #132ba8 #051460 #030333 #000059
let colorBase = '#f9f9f9'
let colorArrows = '#101010'


// =====================================  ARROWS  =====================================

function round(num, decimals=0){
    var k = Math.pow(10, decimals)
    return Math.round((num + Number.EPSILON) * k) / k
}

function rotate(a, theta) {
    return [a[0]*Math.cos(theta) - a[1]*Math.sin(theta), a[0]*Math.sin(theta) + a[1]*Math.cos(theta)];
}

function module(a, b) {
    var v = [b[0]-a[0], b[1]-a[1]]                            // get direction vector
    return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
}

function createArrow(a, b, dist=0.2, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newb = [b[0]-u[0]*dist, b[1]-u[1]*dist]       // place the arrow end point a bit before the real end 
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [a, newb, [newb[0]+s1[0], newb[1]+s1[1]], newb, [newb[0]+s2[0], newb[1]+s2[1]]]
}


function createOutgoingArrow(a, b, dist=0.1, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newa = [a[0]+u[0]*dist, a[1]+u[1]*dist]       // place the arrow start point a bit before the real start 
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [newa, b, [b[0]+s1[0], b[1]+s1[1]], b, [b[0]+s2[0], b[1]+s2[1]]]
}


function createArrowsGeojson(features, center, arrowsIds, incoming, color) {
    var lines = features.filter(x=>arrowsIds.includes(x.id)).map(x => incoming ? [x.centroid, center] : [center, x.centroid]);
    var arrows = lines.map(x => createArrow(x[0], x[1]));
    if (incoming) {
        var arrows = lines.map(x => createArrow(x[0], x[1]));
    } else {
        var arrows = lines.map(x => createOutgoingArrow(x[0], x[1]));
    }
    // var arrows = lines.map(x => x.concat(createArrow(x[0], x[1])));
    // var arrows = lines.map(x => x);
    return { 
        'sourcetype': 'geojson',
        'source': {
            "type": "MultiLineString", 
            "coordinates": arrows
        },
        'below': '', 'type': 'line', 
        'color': color, 'opacity': 1, 
        'line': {'width': 2}
    }
}


function addArrowsToMap(graphDiv, data, locField, valueField, mainAreaId, incoming, color) {
    data = data.sort((a, b)=>a[valueField] < b[valueField] ? 1: -1)
    var topIds = data.slice(0, 10).map(x=>x[locField])  // top 10

    var features = graphDiv.data[0].geojson.features
    var selectedFeature = features.find(y=>y.id == mainAreaId)

    graphDiv.layout.mapbox.layers.push(
        createArrowsGeojson(features, selectedFeature.centroid, topIds, incoming, color))
}

// =====================================  MAPS  =====================================


function createPlainMap(geojson, names) {
    var data = [
        {
            type: "choroplethmapbox",
            locations: geojson.features.map(x=>x.id),
            z: geojson.features.map(x=>1),
            customdata: geojson.features.map(x=>[names[x.id]]),
            geojson: geojson,
            hovertemplate: "%{customdata[0]}<extra></extra>",
            marker: {
                opacity: 0.6,
            },
            showscale: false,
            colorscale: 'Blues',
        }
    ];

    var layout = {
        mapbox: {
            center: {
                lon: -3.717336960173357, lat: 40.495178477814555
                // lon: -8, lat: 36 // shift center to show Canarias
            },
            zoom: 4.5,
            style: 'carto-positron',
        },
        height: 500,
        margin: {"r":0,"t":0,"l":0,"b":0},
    };

    return {data, layout}
}


function createMaps() {
    let url = staticRoute+'data/cnig_provincias.geojson'
    fetch(url).then(response => response.json()).then(geojson => {
        var map = createPlainMap(geojson, province_id_to_name) // same map for both
        Promise.all([
            Plotly.plot(leftMapId, map.data, map.layout, plotly_options),
            Plotly.plot(rightMapId, map.data, map.layout, plotly_options),
        ]).then(gds => {
            let gd1 = gds[0]
            let gd2 = gds[1]
            synchronizeMaps(leftMapId, rightMapId)
            gd1.on('plotly_click', handleClickOnMap)
            gd2.on('plotly_click', handleClickOnMap)

            // Select a default area
            selectAreaMaps(selected_area_id);
        })

    });
}


function resetMaps() {
    selected_area_id = null
    var geojson = document.getElementById(leftMapId).data[0].geojson
    var map = createPlainMap(geojson, province_id_to_name)
    Plotly.react(leftMapId, map.data, map.layout)
    Plotly.react(rightMapId, map.data, map.layout)
}


function handleClickOnMap(e) {

    if (selected_area_id != e.points[0].location) {
        // DIFFERENT AREA THAN THE PREVIOUSLY SELECTED
        selected_area_id = e.points[0].location
        selectAreaMaps(selected_area_id);
    } else {
        // SAME AREA THAN THE PREVIOUSLY SELECTED
        resetMaps()
        selected_area_id = null
        hideMobilityTables()
        $('#time_evolution_mobility_div').hide()
    }
}


function synchronizeMaps(id1, id2) {
    _syncWithOtherMap(id1, id2)
    _syncWithOtherMap(id2, id1)
}


function _syncWithOtherMap(id1, id2) {
    var syncInProgress = false;
    g1 = document.getElementById(id1)
    g2 = document.getElementById(id2)
    g1.on('plotly_relayout', (e) => {
        // update zoom in the other map
        var update = {
            'mapbox.zoom': e['mapbox.zoom'],
            'mapbox.center': e['mapbox.center']
        }
        Plotly.update(id2, {}, update)
    });

    g1.on('plotly_relayouting', (e) => {
        // do not update the zoom too frequently
        if (g2.layout.mapbox.zoom != e['mapbox.zoom']) {
            if (syncInProgress | Math.abs(g2.layout.mapbox.zoom - e['mapbox.zoom']) < 0.2) {
                return
            }
        }
        syncInProgress = true
        var update = {
            'mapbox.zoom': e['mapbox.zoom'],
            'mapbox.center': e['mapbox.center']
        }
        Plotly.update(id2, {}, update).then(gd=>{syncInProgress = false})
    });
}


function selectAreaMaps(area_id) {
    var incoming_filters = {
        "target": area_id,
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
    }
    var outgoing_filters = {
        "source": area_id,
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
    }

    Promise.all([
        fetch(apiURL+'/mitma_mov.daily_mobility_matrix?where='+JSON.stringify(incoming_filters)+'&max_results=500'),
        fetch(apiURL+'/mitma_mov.daily_mobility_matrix?where='+JSON.stringify(outgoing_filters)+'&max_results=500'),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json()
        }));
    }).then(function (data) {
        var incoming_data = data[0]._items
        var outgoing_data = data[1]._items

        incoming_data = incoming_data.filter(x=>x.source != area_id).filter(x=>x.trips>=1)
        outgoing_data = outgoing_data.filter(x=>x.target != area_id).filter(x=>x.trips>=1)

        var incoming_zmax = Math.max.apply(null, incoming_data.map(x=>x.trips));
        var outgoing_zmax = Math.max.apply(null, outgoing_data.map(x=>x.trips));
        var zmax = Math.max(incoming_zmax, outgoing_zmax);

        // update left map
        var leftGraph = document.getElementById(leftMapId)
        updateColorsMap(leftGraph, incoming_data, 'source', 'trips', zmax, colorBase, colorMobility, "Incoming trips", area_id, province_id_to_name, 'Trips going to '+province_id_to_name[selected_area_id])
        addArrowsToMap(leftGraph, incoming_data, 'source', 'trips', area_id, true, colorArrows)
        Plotly.redraw(leftMapId)
       
        // update right map
        var rightGraph = document.getElementById(rightMapId)
        updateColorsMap(rightGraph, outgoing_data, 'target', 'trips', zmax, colorBase, colorMobility, "Outgoing trips", area_id, province_id_to_name, "Trips from "+province_id_to_name[selected_area_id])
        addArrowsToMap(rightGraph, outgoing_data, 'target', 'trips', area_id, false, colorArrows)
        Plotly.redraw(rightMapId)

        // Create datatable
        fillMobilityDataTables(incoming_data, outgoing_data)

        // Create plots
        $('#selected_province_name').text(province_id_to_name[area_id])
        $('#time_evolution_mobility_div').show()
        create_time_evolution_mobility_plot()
        create_time_evolution_mobility_by_person_plot()
    });
}


function updateColorsMap(graphDiv, data, locField, valueField, zmax, colorBase, color, title, area_id, names, valueName) {
    graphDiv.data[0].locations = data.map(x=>x[locField])
    graphDiv.data[0].customdata = data.map(x=>[names[x[locField]], x[valueField].toFixed(0)]);
    graphDiv.data[0].z = data.map(x=>x[valueField])
    graphDiv.data[0].zmax = zmax
    graphDiv.data[0].hovertemplate = "%{customdata[0]}<br>"+valueName+": %{customdata[1]}<extra></extra>";
    graphDiv.data[0].colorscale = [['0.0', colorBase], ['1.0', color]]
    graphDiv.data[0].showscale = true
    graphDiv.data[0].marker.opacity = 0.7
    if (graphDiv.data[0].colobar) {
        graphDiv.data[0].colorbar.title.text = title
    } else {
        graphDiv.data[0].colorbar = {title: {text: title}, thickness: 10, x: 0}
    }

    if (area_id != null) {
        var features = graphDiv.data[0].geojson.features
        var selectedFeature = features.find(y=>y.id == area_id)

        // add polygon for the selected area
        graphDiv.data[1] = {
            type: "choroplethmapbox",
            locations: [area_id],
            geojson: {
                "type": "FeatureCollection",
                "features": features.filter(area => area.id == area_id)
            },
            z: [0],
            customdata: [[names[area_id], area_id]],
            hovertemplate: "%{customdata[0]}<extra></extra>",
            marker: {opacity: 0},
            showscale: false,
            hoverlabel: {align: "left"},
        }

        graphDiv.layout.mapbox.layers = [
            {
                "color": "black",
                "line": {"width": 2.5},
                "opacity": 1,
                "sourcetype": "geojson",
                "type": "line",
                "source": {
                    "type": "FeatureCollection",
                    "features": [selectedFeature]
                }
            },
            {
                "sourcetype": "geojson",
                "type": "fill",
                "opacity": 0.6,
                "color": "yellow",
                "source": {
                    "type": "FeatureCollection",
                    "features": [selectedFeature]
                }
            }
        ];
    }
}


// =====================================  DATATABLE  =====================================
function createEmptyMobilityDataTables() {
    // Hide divs until data is provided
    hideMobilityTables()

    column_names = ['origin', 'destination', 'trips']
    columns_rename = {
        'origin': i18next.t('datatable.origin'),
        'destination': i18next.t('datatable.destination'),
        'trips': i18next.t('datatable.trips'),
    }

    // Incoming and outgoing mobility
    for (var i=0; i<2; i++) {
        var table = ['#incoming_mobility_table', '#outgoing_mobility_table'][i];
        $(table).DataTable({
            data: [],
            columns: column_names.map(function(column) { return {title: columns_rename[column]} }),
            order: [[2, 'desc']],
            columnDefs: [
                { "width": "30%", "targets": [0, 1] },
                { "className": "dt-center", "targets": "_all" },
            ],
            pageLength: 7,
            bLengthChange: false,
            bFilter: false,
            bInfo: false,
            bAutoWidth: true,
        });
    }
}

function fillMobilityDataTables(incoming_data, outgoing_data) {
    // Show the divs that contains the table
    $('#incoming_mobility_table_div').show()
    $('#outgoing_mobility_table_div').show()

    // Incoming mobility from provinces
    var incomingDataSet = [];
    incoming_data.forEach(document => {
        if (province_id_to_name[document['source']] != province_id_to_name[document['target']]) {
            incomingDataSet.push([
                province_id_to_name[document['source']],
                province_id_to_name[document['target']],
                parseInt(document['trips']),
            ]);
        }
    })

    // Outgoing mobility from provinces
    var outgoingDataSet = [];
    outgoing_data.forEach(document => {
        if (province_id_to_name[document['source']] != province_id_to_name[document['target']]) {
            outgoingDataSet.push([
                province_id_to_name[document['source']],
                province_id_to_name[document['target']],
                parseInt(document['trips']),
            ]);
        }
    })

    // Fill the data in the already created table
    var datatable = $('#incoming_mobility_table').DataTable();
    datatable.clear();
    datatable.rows.add(incomingDataSet);
    datatable.columns.adjust();
    datatable.draw();
    var datatable = $('#outgoing_mobility_table').DataTable();
    datatable.clear();
    datatable.rows.add(outgoingDataSet);
    datatable.columns.adjust();
    datatable.draw();
}

function hideMobilityTables() {
    $('#incoming_mobility_table_div').hide()
    $('#outgoing_mobility_table_div').hide()
}


// =====================================  PLOTS  =====================================
function create_time_evolution_mobility_plot() {
    let incoming_mobility_filters = {
        target: selected_area_id,
        target_layer: "cnig_provincias",
        source_layer: "cnig_provincias",
    };
    let incoming_url = apiURL + '/total_incoming_daily_mobility?where=' + JSON.stringify(incoming_mobility_filters)
                                                                        + '&max_results=500';
    let outgoing_mobility_filters = {
        source: selected_area_id,
        source_layer: "cnig_provincias",
        target_layer: "cnig_provincias",
    };
    let outgoing_url = apiURL + '/total_outgoing_daily_mobility?where=' + JSON.stringify(outgoing_mobility_filters)
                                                                        + '&max_results=500';
    var internal_filters = {
        "source": selected_area_id,
        "target": selected_area_id,
        "source_layer": 'cnig_provincias',
        "target_layer": 'cnig_provincias',
    }
    let internal_url = apiURL + '/mitma_mov.daily_mobility_matrix?where=' + JSON.stringify(internal_filters)
                                                                          +'&max_results=500';
    Promise.all([
        fetch(incoming_url),
        fetch(outgoing_url),
        fetch(internal_url),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        var incoming_mobility_data = data[0]._items;
        var outgoing_mobility_data = data[1]._items;
        var internal_mobility_data = data[2]._items;
        var plt_data = [];

        for (var i=0; i<3; i++) {
            var incoming_mobility = [];
            var incoming_dates = [];
            incoming_mobility_data.forEach(doc => {
                incoming_dates.push(doc.date)
                incoming_mobility.push(parseInt(doc.trips))
            })
            var outgoing_mobility = [];
            var outgoing_dates = [];
            outgoing_mobility_data.forEach(doc => {
                outgoing_dates.push(doc.date)
                outgoing_mobility.push(parseInt(doc.trips))
            })

            var internal_mobility = [];
            var internal_dates = [];
            internal_mobility_data.forEach(doc => {
                internal_dates.push(doc.date)
                internal_mobility.push(parseInt(doc.trips))
            })

            plt_data.push({
                name: i18next.t("board_mobility.incoming_mobility"),
                x: incoming_dates,
                y: i==0? incoming_mobility: calculate_smoothy_data(incoming_mobility, i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.trips")+": %{y}<extra></extra>",
                visible: i==0,
                yaxis: 'y1',
            }, {
                name: i18next.t("board_mobility.outgoing_mobility"),
                x: outgoing_dates,
                y: i==0? outgoing_mobility: calculate_smoothy_data(outgoing_mobility, i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.trips")+": %{y}<extra></extra>",
                visible: i==0,
                yaxis: 'y1',
            }, {
                name: i18next.t("board_mobility.internal_mobility"),
                x: internal_dates,
                y: i==0? internal_mobility: calculate_smoothy_data(internal_mobility, i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.trips")+": %{y}<extra></extra>",
                visible: i==0,
                yaxis: 'y2',
            });
        }
        var layout = {
            title: {
                text: i18next.t('board_mobility.incoming_outgoing_internal_mobility'),
                    font: {
                        family: 'Helvetica Neue',
                        size: 24
                    },
                    xref: 'paper',
                    x: 0.05,
            },
            yaxis: {automargin: true, side: 'left', title: {text: i18next.t("board_mobility.mobility_trips")}},
            yaxis2: {automargin: true, overlaying: 'y', side: 'right', title: {text: i18next.t("board_mobility.internal_mobility")}},
            height: 500,
            legend: {x: 1, xanchor: 'right', y: 0.9, yanchor: 'bottom'},
            margin: {r: 50, t: 40, l: 50, b: 50},
            hovermode: 'closest',
            updatemenus: [{
                y: 1,
                yanchor: 'top',
                buttons: [{
                    method: 'restyle',
                    args: ['visible', [true, true, true, false, false, false, false, false, false]],
                    label: i18next.t('plots.daily_mobility')
                }, {
                    method: 'restyle',
                    args: ['visible', [false, false, false, true, true, true, false, false, false]],
                    label: i18next.t('plots.mobility_7_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [false, false, false, false, false, false, true, true, true]],
                    label: i18next.t('plots.mobility_14_days')
                }],
                direction: "right",
                x: 0.01,
                xanchor: "left",
                yanchor: "top",
                legend: {traceorder: 'normal'},
            }],
        };

        Plotly.newPlot('time_evolution_mobility', plt_data, layout, ply_config)
    }).catch(function (error) {
        $('#time_evolution_mobility_div').hide();
        console.log('Time evolution mobility plot error.');
    })
}

function sumArrays(arrays) {
    const n = arrays.reduce((max, xs) => Math.max(max, xs.length), 0);
    const result = Array.from({ length: n });
    return result.map((_, i) => arrays.map(xs => xs[i] || 0).reduce((sum, x) => sum + x, 0));
}

function create_time_evolution_mobility_by_person_plot() {
    var fetch_list = [];
    for (var i=0; i<4; i++) {
        let filters = {
            id: selected_area_id,
            layer: "cnig_provincias",
            type: "zone_movements",
            viajes: i!=3? i: -1,
        }
        let url = apiURL + '/layers.data.consolidated?where=' + JSON.stringify(filters)
                                                              + '&max_results=500'
                                                              + '&sort=[("date", 1)]';
        fetch_list.push(fetch(url));
    };

    Promise.all(fetch_list).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        var movility_data = [];
        var dates = [];
        data.forEach(element => {
            dates.push(element._items.map(function (item) { return item.date}));
            movility_data.push(element._items.map(function (item) {return item.personas}));
        })
        var total_population = sumArrays(movility_data)

        var plt_data = [];
        for (var i=0; i<3; i++) {
            plt_data.push({
                name: "0 " + i18next.t("board_mobility.trips"),
                x: dates[0],
                y: i==0? movility_data[0]: calculate_smoothy_data(movility_data[0], i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.people")+": %{y}<extra></extra>",
                visible: i==0,
            }, {
                name: "1 " + i18next.t("board_mobility.trips"),
                x: dates[1],
                y: i==0? movility_data[1]: calculate_smoothy_data(movility_data[1], i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.people")+": %{y}<extra></extra>",
                visible: i==0,
            }, {
                name: "2 " + i18next.t("board_mobility.trips"),
                x: dates[2],
                y: i==0? movility_data[2]: calculate_smoothy_data(movility_data[2], i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.people")+": %{y}<extra></extra>",
                visible: i==0,
            }, {
                name: i18next.t("board_mobility.more_than_2_trips"),
                x: dates[3],
                y: i==0? movility_data[3]: calculate_smoothy_data(movility_data[3], i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.people")+": %{y}<extra></extra>",
                visible: i==0,
            }, {
                name: i18next.t("board_mobility.total_population"),
                x: dates[3],
                y: i==0? total_population: calculate_smoothy_data(total_population, i==1? 7: 14),
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>"+i18next.t("board_mobility.people")+": %{y}<extra></extra>",
                visible: i==0,
            })
        }

        var button_option_true = new Array(5).fill(true)
        var button_option_false = new Array(5).fill(false)
        var layout = {
            title: {
                text: i18next.t('board_mobility.trips_made_per_person'),
                    font: {
                        family: 'Helvetica Neue',
                        size: 24
                    },
                    xref: 'paper',
                    x: 0.05,
            },
            yaxis: {automargin: true, side: 'left', title: {text: i18next.t("board_mobility.people")}},
            height: 500,
            legend: {x: 1, xanchor: 'right', y: 0.7, yanchor: 'bottom'},
            margin: {r: 50, t: 40, l: 50, b: 50},
            hovermode: 'closest',
            updatemenus: [{
                buttons: [{
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_true, button_option_false, button_option_false)],
                    label: i18next.t('plots.daily_value')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_false, button_option_true, button_option_false)],
                    label: i18next.t('plots.7_day_average_value')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_false, button_option_false, button_option_true)],
                    label: i18next.t('plots.14_day_average_value')
                }],
                x: 0.01,
                y: 0.9,
                xanchor: "left",
                yanchor: "top",
                legend: {traceorder: 'normal'},
            }],
        };

        Plotly.newPlot('time_evolution_mobility_by_person', plt_data, layout, ply_config)
    })
    .catch(function (error) {
        $('#time_evolution_mobility_div').hide();
        console.log('Time evolution mobility per person plot error.', error);
    })
}

// =====================================  DATEPICKER  =====================================
function onChangeDate(new_date_str) {
    date_str = new_date_str
    document.getElementById('selected-date').innerHTML = date_str
    if (selected_area_id == null) {
        // If no area is selected, create the maps from scratch (using new date)
        resetMaps()
    } else {
        selectAreaMaps(selected_area_id);
    }
}

configCalendar(min_date, max_date, onChangeDate);


// =====================================  INITIALIZATION  =====================================

i18next.on('initialized', function(options) {
    createMaps();

    createEmptyMobilityDataTables();
})
