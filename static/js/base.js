// =====================================  GLOBAL VARIABLES  =====================================
var province_to_ccaa = {38:"05",35:"05",39:"06","03":"10",12:"10",46:"10","06":"11",10:"11",51:"18","02":"08",19:"08",13:"08",16:"08",45:"08",30:"14",15:"12",36:"12",27:"12",32:"12",52:"19",33:"03",26:"17","09":"07","05":"07",34:"07",24:"07",42:"07",40:"07",37:"07",49:"07",47:"07","07":"04",48:"16","01":"16",20:"16",44:"02",22:"02",50:"02",17:"09","08":"09",25:"09",43:"09",28:"13",31:"15",21:"01",11:"01",14:"01",18:"01","04":"01",29:"01",23:"01",41:"01"};
var province_centroids = {38:[-17.01908923931924,28.312799413043724],35:[-14.546977440514675,28.36589401112925],"02":[-1.9805422386602272,38.82535944844695],"07":[2.9113833473794144,39.573997479762475],21:[-6.8289294474458355,37.577130648046385],15:[-8.463990247890797,43.125599850458244],11:[-5.760281046735342,36.55345600588369],"03":[-.5685298929690359,38.478586992697785],14:[-4.8095282226771605,37.992761727844986],18:[-3.267472909430516,37.3126223893198],"09":[-3.5859360102830755,42.368569205557264],17:[2.6736455216331305,42.12786462428146],30:[-1.4849777665021868,38.002032326940295],"06":[-6.141505367536164,38.709853939564056],12:[-.1467282796100414,40.2415957081869],"04":[-2.344681259015228,37.19561813422608],19:[-2.623916123370475,40.81334027203937],13:[-3.827800344901022,38.92548468731909],"05":[-4.945481472274611,40.57113819104595],10:[-6.1607567863747965,39.711877905510775],"08":[1.9835975053226584,41.731162512078434],52:[-2.9516157472353126,35.28959952131277],16:[-2.195456535729252,39.8960477471854],36:[-8.461337824584824,42.43559590164071],28:[-3.717336960173357,40.495178477814555],25:[1.0476185984928572,42.04383901748333],29:[-4.72589963767593,36.81366990767922],27:[-7.445803599985688,43.011872770443816],34:[-4.535802047542971,42.371663130774515],23:[-3.441734174375318,38.01624479649859],39:[-4.02920937852486,43.197828030543924],44:[-.815827371119849,40.66122942756358],41:[-5.682849474318119,37.435681881984614],22:[-.0730587994840405,42.20311331399378],24:[-5.839836135817316,42.619984018564836],26:[-2.5170817709587037,42.274855182085034],42:[-2.5888097845344324,41.62072011372254],33:[-5.99341170846315,43.2924346451998],31:[-1.6461594504360129,42.66717341880263],32:[-7.592487668054875,42.19642758688591],43:[.8177348120966765,41.0877493500896],40:[-4.0543988632302925,41.170919810088066],45:[-4.14788589884475,39.793734282537834],37:[-6.065433538003191,40.80494884223812],46:[-.8007235537003341,39.370096874281735],49:[-5.980394099657101,41.72718319902787],50:[-1.0643426504426452,41.62055515348005],48:[-2.851895469148218,43.23792631285875],47:[-4.847381317805473,41.63424521724669],"01":[-2.720520596347652,42.83502645296802],51:[-5.339071736624627,35.89204135639479],20:[-2.1942539786341024,43.143585079924875]};
var ccaa_data_sublayer = {"07":"zbs_07","09":"abs_09",13:"zon_bas_13",15:"zbs_15",16:"oe_16"};
var layer_to_ev = {cnig_provincias:"ES.covid_cpro",cnig_ccaa:"ES.covid_cca",zbs_07:"07.covid_abs",abs_09:"09.covid_abs",zon_bas_13:"13.covid_abs",zbs_15:"15.covid_abs",oe_16:"16.covid_abs"};


function initialize_i18next() {
    i18next
        .use(i18nextHttpBackend)
        .use(i18nextBrowserLanguageDetector)
        .init({
            fallbackLng: 'en',
            backend: {
                loadPath: staticRoute + 'locales/{{lng}}/{{ns}}.json',
            },
            detection: {
                order: ['localStorage'],
                caches: ['localStorage'],
            }
        }, function(err, t) {
            jqueryI18next.init(i18next, $);
            $('.translatable').localize();
        });
}

function changeLng(lng) {
    // Change the language and refresh the elements
    i18next.changeLanguage(lng, function() {
        $('.translatable').localize();
    });
}

function initialize_selectpicker() {
    // Get the language from the local storage and mark this option
    var language = window.localStorage.getItem('i18nextLng') || 'en';
    $('#languagepicker option:contains(' + language + ')').attr('selected', true);

    // Give styles to selectpicker
    $('.selectpicker').selectpicker();

    // Change the language when the select changes.
    $("#languagepicker").on("changed.bs.select",  function(e, clickedIndex, newValue, oldValue) {
        // When changing the selectpicker option, call the i18next.changeLanguage and update the content
        var language = this.value;
        changeLng(language);
    });
};

function calculate_smoothy_data(data, n_elements) {
    // Given an array of elements `data`, return a windowed mean of those elements (group by `window_size`)
    var result = [];
    for (var i = 0; i < data.length; i++) {
        var smoothy_value = 0;
        for (j = 0; j < n_elements; j++) {
            if (i - j < 0) {
                break;
            } else {
                smoothy_value += data[i-j];
            }
        }
        result.push(smoothy_value/(j));
    }
    return result;
}

function setup_accordion_behaviour() {
    $('.panel-collapse').on('shown.bs.collapse', function () {
        const yOffset = -110;  // Offset due to the header
        const y = $(this)[0].getBoundingClientRect().top + window.pageYOffset + yOffset;
        window.scrollTo({top: y, behavior: 'smooth'});
    });
}

function setup_sidebar_behaviour() {
    // Scroller
    $("#sidebar").mCustomScrollbar({
        theme: "minimal",
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
}

function configCalendar(custom_min_date, custom_max_date, on_change_date_function) {
    $('#calendar').show();
    $('#calendar').datepicker({
        format: 'yyyy-mm-dd',
        // language: "nl",
        // calendarWeeks: true,
        todayHighlight: true,
        // startDate: Date.parse(min_date),
        // maxDate: Date.parse(max_date),
        startDate: custom_min_date,
        endDate: custom_max_date,
        weekStart: 1,
    }).on('changeDate', function (e) {
        on_change_date_function(e.format('yyyy-mm-dd'));
    });
    $('#calendar').datepicker('update', date_str);
}

function mark_active_tabs() {
    // Get the active tabs (side and top bar) from the template and add `active` class to them
    activeTabs.forEach(tab => {
        $('#' + tab).addClass("active")
    })
}

function redirect_to(base_url, include_date_str=false) {
    // Redirect to an url, and if include_date_str is true, and if the variable is available
    // on the template.

    window.location.href = include_date_str && date_str? base_url + date_str: base_url;
}

// Initializing items
initialize_i18next();
initialize_selectpicker();
setup_accordion_behaviour();
setup_sidebar_behaviour();
mark_active_tabs();