
function buildMap(geojsonData) {
	var coords = [39.58107878703854, -3.0046948270236196]

	var map = L.map('map').setView(coords, 5);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/light-v9',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(map);
	// map.attributionControl.addAttribution('Population data &copy; <a href="http://census.gov/">US Census Bureau</a>');
	return map;
}


function addOnHoverInfo(map) {
	// control that shows state info on hover
	var info = L.control();

	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		this._div.innerHTML = '<h4>US Population Density</h4>' +  (props ?
			'<b>' + props.name + '</b><br />' + props.density + ' people / mi<sup>2</sup>'
			: 'Hover over a state');
	};

	info.addTo(map);
}


function setGeojsonData(map, geojsonData, onClickPolygon) {
	map.eachLayer(function (layer) {
		// console.log(layer);
		if (layer.type == 'geojson') {
		    map.removeLayer(layer);
		}
	});
	// get color depending on population density value
	// function getColor(d) {
	// 	return d > 1000 ? '#800026' :
	// 			d > 500  ? '#BD0026' :
	// 			d > 200  ? '#E31A1C' :
	// 			d > 100  ? '#FC4E2A' :
	// 			d > 50   ? '#FD8D3C' :
	// 			d > 20   ? '#FEB24C' :
	// 			d > 10   ? '#FED976' :
	// 						'#FFEDA0';
	// }
	// function getColor(d) {
	// 	return '#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6);
	// }

	var getColor = chroma.scale(['#FFEDA0', '#800026']).domain([1, 100000], 7, 'log');
	// var getColor = (d) => '#800026'
	

	function style(feature) {
		return {
			weight: 2,
			opacity: 1,
			color: '#999',
			// dashArray: '3',
			fillOpacity: 0.7,
			// fillColor: getColor(feature.properties.id),
			fillColor: getColor(1),
		};
	}

	// function highlightFeature(e) {
	// 	var layer = e.target;

	// 	layer.setStyle({
	// 		weight: 3,
	// 		color: '#666',
	// 		dashArray: '',
	// 		fillOpacity: 0.7
	// 	});

	// 	if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
	// 		layer.bringToFront();
	// 	}

	// 	info.update(layer.feature.properties);
	// }

	var geojsonLayer;

	// function resetHighlight(e) {
	// 	geojson.resetStyle(e.target);
	// 	info.update();
	// }

	// function zoomToFeature(e) {
	// 	map.fitBounds(e.target.getBounds());
	// }

	// function onEachFeature(feature, layer) {
	// 	layer.on({
	// 		mouseover: highlightFeature,
	// 		mouseout: resetHighlight,
	// 		click: zoomToFeature
	// 	});
	// }
	var selectedPolygon = null;

	function markSelected() {
		if (selectedPolygon) {		
			selectedPolygon.target.setStyle({
				selected: true,
				weight: 4,
				color: '#666',
				dashArray: '',
				fillOpacity: 0.7
			});
			selectedPolygon.target.bringToFront();
		}
	}

	function onClick(e) {
		if (selectedPolygon) {
			geojsonLayer.resetStyle(selectedPolygon.target);
		}
		selectedPolygon = e;
		markSelected();
		onClickPolygon(e);
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: (e) => {
				e.target.setStyle({
					selected: false,
					weight: 3,
					color: '#666',
					dashArray: '',
					fillOpacity: 0.7
				});
				if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
					layer.bringToFront();
					markSelected();
				}
			},
			mouseout: (e) => {
				if (!e.target.options.selected) {
					geojsonLayer.resetStyle(e.target);
				}
			},
			click: onClick,
		});
	}

	geojsonLayer = L.geoJson(geojsonData, {
		style: style,
		onEachFeature: onEachFeature
	})
	geojsonLayer.type = 'geojson';
	geojsonLayer.addTo(map);
}


function addLegend(map) {

	var legend = L.control({position: 'bottomright'});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend'),
			grades = [0, 10, 20, 50, 100, 200, 500, 1000],
			labels = [],
			from, to;

		for (var i = 0; i < grades.length; i++) {
			from = grades[i];
			to = grades[i + 1];

			labels.push(
				'<i style="background:' + getColor(from + 1) + '"></i> ' +
				from + (to ? '&ndash;' + to : '+'));
		}

		div.innerHTML = labels.join('<br>');
		return div;
	};

	legend.addTo(map);
}


function generateTable(table, data) {
	// empty previous table
	table.innerHTML = "";
	// console.log(data);
	// header
	let columns = Object.keys(data[0]);
	let thead = table.createTHead();
	let row = thead.insertRow();
	for (let key of columns) {
		let th = document.createElement("th");
		let text = document.createTextNode(key);
		th.appendChild(text);
		row.appendChild(th);
	}
	// rows
	let tbody = table.createTBody();
	for (let element of data) {
	    let row = tbody.insertRow();
	    for (key in element) {
	        let cell = row.insertCell();
	        let text = document.createTextNode(element[key]);
	        cell.appendChild(text);
	    }
	}
}


function onSelectPolygon(e) {
	// console.log(e);
	// console.log(e.target.feature.id);
	document.getElementById('info').innerText = e.target.feature.id;

	fetch("/data/"+e.target.feature.id)
	  .then(response => response.json())
	  .then(json => {
	  	// console.log(json);
	  	var table_container = document.getElementById('table_container');
	  	var table = document.createElement("table");
	  	table.setAttribute("id", "risk_table");
	  	table_container.innerHTML = '';
	  	table_container.appendChild(table)
	  	generateTable(table, json['data']);
	  	$('#risk_table').DataTable();
	  });
}


Promise.all([
	fetch('/static/data/cnig_ccaa.geojson'),
	fetch('/static/data/cnig_provincias.geojson'),
	fetch('/static/data/sublayer.geojson'),
]).then(function (responses) {
	return Promise.all(responses.map(function (response) {
		return response.json();
	}));
}).then(function (data) {
	var ccaaGeojson = data[0] 
	var provinciasGeojson = data[1] 
	var sublayerGeojson = data[2] 

	var map = buildMap(ccaaGeojson);
	setGeojsonData(map, ccaaGeojson, onSelectPolygon);

	map.on('zoomend', function(e) {
		var z = e.target._zoom;
		// console.log(z);
		var geojsonData = z > 6 ? sublayerGeojson :
						  z > 5  ? provinciasGeojson :
						  ccaaGeojson;
		setGeojsonData(map, geojsonData, onSelectPolygon);
	})

}).catch(function (error) {
	// if there's an error, log it
	console.log(error);
});

