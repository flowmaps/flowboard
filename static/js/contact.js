function redirect_to_mailto() {
    // Creating the href link based on the information within the form
    var full_name = document.getElementById('fname').value
    var organisation = document.getElementById('org').value
    var subject = 'Contact Flow Maps: ' + full_name + ' (' + organisation + ')'
    var body = document.getElementById('subject').value
    var email_to = "flowmaps@bsc.es"
    var href_url = "mailto:" + email_to + '?subject=' + subject + '&body=' + body
	window.location.href = href_url;
}
