// =====================================  GLOBAL VARIABLES  =====================================
var provincesGeojson = null;
var localGeojson = null;
var ply_config = { displayModeBar: false }


// =====================================  CONSTANTS  =====================================
// First element of the list for incoming tab, the other for the outgoing
var province_id_list = ['08', '08'];
var local_layer_id_list = [null, null];
var ev_list = [null, null];
var province_coords = null;
var province_lat = 0;
var province_lon = 0;



// =====================================  AESTHETIC CHANGES  =====================================
// COLORS
let colorOutgoing = '#240047'
let colorOutgoingArrows = '#240047'
let colorIncoming = '#950000'
let colorIncomingArrows = '#300b0b' // #575757 #330404
let colorIncidence = '#000059' // #050559 #03032e #030333 #000059
let colorBase = '#e9e9e9'

// TABS
function draw_plotly_on_show_tab() {
    // Initialize the Plotly graphs when the tab is activated in order to avoid an issue that happens
    // when Plotly doesn't know exactly the size of the available div (it's hidden)

    $('#incoming-tab').on('shown.bs.tab', function (e) {
        if ( ! $( '#incomingRiskMap' ).hasClass( "js-plotly-plot" ) ) {
            createIncomingRiskMap();
        }
    });

    $('#outgoing-tab').on('shown.bs.tab', function (e) {
        if ( ! $( '#outgoingRiskMap' ).hasClass( "js-plotly-plot" ) ) {
            createOutgoingRiskMap();
        }
    });
}

draw_plotly_on_show_tab();


// =====================================  MAP MANAGEMENT  =====================================
function round(num, decimals=0){
    var k = Math.pow(10, decimals)
    return Math.round((num + Number.EPSILON) * k) / k
}

function rotate(a, theta) {
    return [a[0]*Math.cos(theta) - a[1]*Math.sin(theta), a[0]*Math.sin(theta) + a[1]*Math.cos(theta)];
}

function module(a, b) {
    var v = [b[0]-a[0], b[1]-a[1]]                            // get direction vector
    return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
}

function createArrow(a, b, dist=0.2, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newb = [b[0]-u[0]*dist, b[1]-u[1]*dist]       // place the arrow end point a bit before the real end 
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [a, newb, [newb[0]+s1[0], newb[1]+s1[1]], newb, [newb[0]+s2[0], newb[1]+s2[1]]]
}

function createOutgoingArrow(a, b, dist=0.1, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newa = [a[0]+u[0]*dist, a[1]+u[1]*dist]       // place the arrow start point a bit before the real start 
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [newa, b, [b[0]+s1[0], b[1]+s1[1]], b, [b[0]+s2[0], b[1]+s2[1]]]
}

function createArrowsGeojson(features, centerId, arrowsIds, incoming=true) {
    var center = features.find(x=> x.id == centerId).centroid;
    var lines = features.filter(x=>arrowsIds.includes(x.id)).map(x => incoming ? [x.centroid, center] : [center, x.centroid]);
    var arrows = lines.map(x => createArrow(x[0], x[1]));
    if (incoming) {
        var color = colorIncomingArrows;
        var arrows = lines.map(x => createArrow(x[0], x[1]));
    } else {
        var color = colorOutgoingArrows;
        var arrows = lines.map(x => createOutgoingArrow(x[0], x[1]));
    }
    // var arrows = lines.map(x => x.concat(createArrow(x[0], x[1])));
    // var arrows = lines.map(x => x);
    return { 
        'sourcetype': 'geojson',
        'source': {
            "type": "MultiLineString", 
            "coordinates": arrows
        },
        'below': '', 'type': 'line', 
        'color': color, 'opacity': 1, 
        'line': {'width': 2}
    }
}

// Incoming
function createIncomingRiskMap() {
    // Start creating local risk map
    local_layer_id_list[0] = ccaa_data_sublayer[province_to_ccaa[province_id_list[0]]]
    ev_list[0] = layer_to_ev[local_layer_id_list[0]]
    province_coords = province_centroids[province_id_list[0]]
    createLocalIncomingRiskMap();
    hide_detailedLocalIncomingRisk_table();

    // Incoming risk to province_id
    var risk_filters = {
        "target": province_id_list[0],
        "date": date_str,
        "source_layer": "cnig_provincias",
        "target_layer": "cnig_provincias",
        "ev": "ES.covid_cpro",
    }

    // Total cases of the selected province
    var cases_filters = {
        "type": "covid19",
        "ev": "ES.covid_cpro",
        "date": date_str,
        "layer": "cnig_provincias",
        "id": province_id_list[0],
    }

    Promise.all([
        fetch(staticRoute+'data/cnig_provincias.geojson'),
        fetch(apiURL+'/incoming_risk/?where='+JSON.stringify(risk_filters)),
        fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(cases_filters)+"&max_results=1"),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        // Geojson management
        var provinciasGeojson = data[0];
        var provincesNames = {};
        provinciasGeojson.features.forEach(feat => {
            provincesNames[feat.id] = feat.name;
        })
        var provinceGeojson = {
            "type": "FeatureCollection",
            "features": provinciasGeojson.features.filter(province => province.id == province_id_list[0])
        }

        // Risk management
        var provinciasData = data[1];
        var incomingRisk = provinciasData._items;
        incomingRisk = incomingRisk.sort( (a, b) => a.incoming_risk < b.incoming_risk ? 1: -1);
        incomingRisk = incomingRisk.filter(a => a.target != a.source); // filter out same origin
        incomingRisk = incomingRisk.filter(a => a.incoming_risk > 0.01);
        var province_total_incoming_risk = round(incomingRisk.reduce((a, b) => a + b.incoming_risk, 0), 2);

        // Cases management
        var provinceCasesData = data[2]._items[0];

        // Insert province name into hovertemplate
        $('.incoming_province_name').text(provincesNames[province_id_list[0]])

        // Generate the fields for data
        var data = [{
            type: "choroplethmapbox",
            locations: incomingRisk.map(x=>x.source),
            z: incomingRisk.map(x=>x.incoming_risk),
            customdata: incomingRisk.map(x=>[
                provincesNames[x.source],
                round(x.trips),
                round(x.incoming_risk, 2)]),
            geojson: provinciasGeojson,
            hovertemplate: "%{customdata[0]}<extra></extra>",
            marker: {
                opacity: 0.6,
            },
            colorbar: {
                title: {
                    text: "Incoming Risk",
                    side: "right"
                },
            },
            colorscale: [['0.0', colorBase], ['1.0', colorIncoming]],
            hoverlabel: {align: "left"},
        }, {
            type: "choroplethmapbox",
            locations: [province_id_list[0]],
            z: [0],
            customdata: [[provincesNames[province_id_list[0]], provinceCasesData]],
            geojson: provinceGeojson,
            hovertemplate: "Selected province: %{customdata[0]}<extra></extra>",
            marker: {
                opacity: 0,
            },
            showscale: false,
            hoverlabel: {align: "left"},
        }];
        
        var layout = {
            mapbox: {
                center: {
                    lon: -3.717336960173357, lat: 40.495178477814555
                },
                zoom: 5,
                style: 'carto-positron',
                layers: [{
                    "color": "black",
                    "line": {"width": 2.5},
                    "opacity": 1,
                    "sourcetype": "geojson",
                    "type": "line",
                    "source": {
                        "type": "FeatureCollection",
                        "features": [provinciasGeojson.features.find(y=>y.id == province_id_list[0])]
                    }
                },
                {                   
                    "sourcetype": "geojson",
                    "type": "fill",
                    "opacity": 0.5,
                    "color": "yellow",
                    "source": {
                        "type": "FeatureCollection",
                        "features": [provinciasGeojson.features.find(y=>y.id == province_id_list[0])]
                    }
                }
                ]
            },
            // width: 800,
            height: 500,
            margin: {"r":0,"t":0,"l":0,"b":0},
        };


        // Build arrows
        var topRiskIds = incomingRisk.slice(0, 10).map(x=>x.source);  // the risk sources are already sorted in the database
        layout.mapbox.layers.push(
            createArrowsGeojson(provinciasGeojson.features, province_id_list[0], topRiskIds, incoming=true))

        // Create plot
        Plotly.newPlot('incomingRiskMap', data, layout, ply_config).then(gd => {
            gd.on('plotly_click', (e) => {
                if (e.points[0].location != province_id_list[0]) {
                    province_id_list[0] = e.points[0].location;

                    // Create the maps
                    createIncomingRiskMap();
                    create_linegraph();

                    // Start creating the province plots
                    $('#incomingrisktopgraph').empty();
                    $('#loading-incomingrisktopgraph').show();
                    create_incoming_risk_topgraph();
                }
            });

            gd.on('plotly_hover', (e) => {
                var customdata = e.points[0].customdata;
                if (e.points[0].location != province_id_list[0]) {
                    var text = customdata[0] + "<br>" +
                               "Risk from "+customdata[0]+": "+customdata[2]+"<br>" +
                               "Trips: "+customdata[1]+"<br>"
                               
                } else {
                    var provinceCasesData = customdata[1];
                    var text = customdata[0] + "<br>" +
                               "Total Incoming Risk: "+province_total_incoming_risk+"<br>"+
                               "New Cases:<br>" +
                                 "- Incidence: " + provinceCasesData.new_cases + "<br>" +
                               "Accumulated Cases:<br>" +
                                 "- Incidence (7 days): " + provinceCasesData.active_cases_7 + "<br>" +
                                 "- Incidence (14 days): " + provinceCasesData.active_cases_14 + "<br>"
                }
                $('#hoverinfo_incomingriskmap')[0].innerHTML = text;
                $('#hoverinfo_incomingriskmap')[0].style.visibility = 'visible';
            })

            gd.on('plotly_unhover', (e) => {
                $('#hoverinfo_incomingriskmap')[0].style.visibility = 'hidden';
            });
        });

        // Update datatable
        update_detailedIncomingRisk_table(provinciasData._items, provincesNames);

    })
    /*.catch(function (error) {
        // Hide div in case of fail
        $('#incoming_risk_div').hide()
        $('#local_incoming_risk_div').hide()
        console.log('Incoming risk map not available.')
    });*/
}

function createLocalIncomingRiskMap() {
    if (! local_layer_id_list[0]) {
        console.log('no local data available')
        $("#localIncomingRiskMap").text('Sorry, we do not have high resolution Covid-19 data for this CCAA.');
        return;
    } else {
        $("#localIncomingRiskMap").empty();
    }

    Plotly.purge("localIncomingRiskMap");

    // province_id and ccaa_id are setted by flask in the html
    let area_id = null;

    var filters = {
        "type": "covid19",
        "date": date_str,
        "ev": ev_list[0],
    }

    Promise.all([
        fetch(staticRoute+'data/hires_province_'+province_id_list[0]+'.geojson'),
        fetch(staticRoute+'data/cnig_provincias.geojson'),
        fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters)+"&max_results=500"),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        var geojson = data[0];
        var provincesGeojson = data[1];
        var covidData = data[2]['_items'];
        var covidDataIndex = {}; covidData.forEach(d => {covidDataIndex[d.id] = d})

        var data = [
            {
                type: "choroplethmapbox",
                locations: geojson.features.map(x=>x.id),
                z: geojson.features.map(x=>(covidDataIndex[x.id] || {}).active_cases_14 || 0),
                customdata: geojson.features.map(x=>[x.id, x.name]),
                geojson: geojson,
                hovertemplate: "Name: %{customdata[1]},<br>Area id: %{customdata[0]},<br>Incidence (14 days): %{z}<extra></extra>",
                marker: {
                    opacity: 0.6,
                },
                colorbar: {title: {text: "Incidence (14 days)"}},
                colorscale: [['0.0', colorBase], ['1.0', colorIncidence]],
                hoverlabel: {align: "left"},
            },
            {
                type: "choroplethmapbox",
                locations: [],
                z: provincesGeojson.features.map(x=>1),
                geojson: provincesGeojson,
                marker: {
                    opacity: 0.6,
                },
                colorbar: {title: {text: "Incidence (14 days)"}},
                colorscale: [['0.0', colorBase], ['1.0', colorIncidence]],
                hoverlabel: {align: "left"},
            },
        ];

        var layout = {
            mapbox: {
                center: {
                    lon: province_coords[0], lat: province_coords[1]
                },
                zoom: 7,
                style: 'carto-positron',
            },
            height: 500,
            margin: {"r":0,"t":0,"l":0,"b":0},
        };

        Plotly.plot('localIncomingRiskMap', data, layout, ply_config).then(gd => {
            gd.on('plotly_click', (e) => {

                if (e.points[0].curveNumber == 1) {
                    // clicked on a province
                    // alert("You have clicked on a province (if you wish, you can select a different province in the upper map)")
                    if (confirm("You have clicked on a different province. Do you want to continue?")) {
                        province_id_list[0] = e.points[0].location;
                        createIncomingRiskMap();

                        // Start creating the province plots
                        $('#incomingrisktopgraph').empty();
                        $('#loading-incomingrisktopgraph').show();
                        create_incoming_risk_topgraph();
                    }
                    return;
                }

                if (area_id != e.points[0].location) {
                    // DIFFERENT AREA THAN THE PREVIOUSLY SELECTED
                    area_id = e.points[0].location;
                    selectAreaLocalIncomingRiskMap(area_id);
                } else {
                    // SAME AREA THAN THE PREVIOUSLY SELECTED
                    hide_detailedLocalIncomingRisk_table();
                    createLocalIncomingRiskMap();
                }
            });
        });

    }).catch(function (error) {
        // Hide div in case of fail
        $('#local_incoming_risk_div').hide()
        console.log('Local incoming risk map not available.')
    });
}

function selectAreaLocalIncomingRiskMap(area_id) {
    var filters = {
        "target": area_id,
        "date": date_str,
        "source_layer": local_layer_id_list[0],
        "target_layer": local_layer_id_list[0],
        "ev": ev_list[0],
    }
    var filters2 = {
        "target": area_id,
        "date": date_str,
        "source_layer": 'cnig_provincias',
        "target_layer": local_layer_id_list[0],
        "ev": 'ES.covid_cpro',
    }
    Promise.all([
        fetch(apiURL+'/incoming_risk/?where='+JSON.stringify(filters)),
        fetch(apiURL+'/incoming_risk/?where='+JSON.stringify(filters2)),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        if ('error' in data[0]) {
            alert(JSON.stringify(data[0])); return;
        }
        if ('error' in data[1]) {
            alert(JSON.stringify(data[1])); return;
        }
        if (!Array.isArray(data[0]._items) || !data[0]._items.length) {
            alert('Sorry, there is no data for the selected area.');
            return;
        }
        var localIncomingRisk = data[0]._items;
        var provincesIncomingRisk = data[1]._items;
        localIncomingRisk = localIncomingRisk.filter(a=>a.incoming_risk > 0.01)
        localIncomingRisk = localIncomingRisk.sort((a, b)=>a.incoming_risk < b.incoming_risk ? 1: -1)
        provincesIncomingRisk = provincesIncomingRisk.filter(a=>a.incoming_risk > 0.01)
        provincesIncomingRisk = provincesIncomingRisk.sort((a, b)=>a.incoming_risk < b.incoming_risk ? 1: -1)
        provincesIncomingRisk = provincesIncomingRisk.filter(a=>a.source != province_id_list[0])

        var graphDiv = document.getElementById('localIncomingRiskMap');
        var features = graphDiv.data[0].geojson.features;
        var selectedFeature = features.find(y=>y.id == area_id);
        var names = {}; features.forEach(feat => {names[feat.id] = feat.name;})
        var provincesNames = {}; graphDiv.data[1].geojson.features.forEach(feat => {provincesNames[feat.id] = feat.name;})

        // update local risk trace
        graphDiv.data[0].locations = localIncomingRisk.map(x=>x.source);
        graphDiv.data[0].customdata = localIncomingRisk.map(x=>[names[x.source], round(x.trips), round(x.incoming_risk, 2)]);
        graphDiv.data[0].z = localIncomingRisk.map(x=>x.incoming_risk);
        graphDiv.data[0].colorbar.title.text = "Risk"
        graphDiv.data[0].hovertemplate = "Name: %{customdata[0]}<br>Trips: %{customdata[1]}<br>Risk: %{customdata[2]}<extra></extra>";
        graphDiv.data[0].colorscale = [['0.0', colorBase], ['1.0', colorIncoming]];
        graphDiv.data[0].colorbar = {title: {text: "Incoming Risk"}};

        // update provinces risk trace
        graphDiv.data[1].locations = provincesIncomingRisk.map(x=>x.source);
        graphDiv.data[1].customdata = provincesIncomingRisk.map(x=>[provincesNames[x.source], round(x.trips), round(x.incoming_risk, 2)]);
        graphDiv.data[1].z = provincesIncomingRisk.map(x=>x.incoming_risk);
        graphDiv.data[1].colorbar.title.text = "Risk"
        graphDiv.data[1].hovertemplate = "Name: %{customdata[0]}<br>Trips: %{customdata[1]}<br>Risk: %{customdata[2]}<extra></extra>";
        graphDiv.data[1].colorscale = [['0.0', colorBase], ['1.0', colorIncoming]];
        graphDiv.data[1].colorbar = {title: {text: "Incoming Risk"}};
        graphDiv.data[1].showscale = false;
        graphDiv.data[1].zmax = Math.max(...graphDiv.data[0].z)
        graphDiv.data[1].zmin = 0

        // Adding the same area_id that is selected
        graphDiv.data[2] = {
            type: "choroplethmapbox",
            locations: [area_id],
            geojson: {
                "type": "FeatureCollection",
                "features": graphDiv.data[0].geojson.features.filter(area => area.id == area_id)
            },
            z: [0],
            customdata: [[names[area_id], area_id]],
            hovertemplate: "Name: %{customdata[0]}<br>Area id: %{customdata[1]}<extra></extra>",
            marker: {
                opacity: 0,
            },
            showscale: false,
            hoverlabel: {align: "left"},
        }

        graphDiv.layout.mapbox.layers = [
            {
                "color": "black",
                "line": {"width": 2.5},
                "opacity": 1,
                "sourcetype": "geojson",
                "type": "line",
                "source": {
                    "type": "FeatureCollection",
                    "features": [selectedFeature]
                }
            },
            {
                "sourcetype": "geojson",
                "type": "fill",
                "opacity": 0.5,
                "color": "yellow",
                "source": {
                    "type": "FeatureCollection",
                    "features": [selectedFeature]
                }
            }
        ];

        // Build arrows
        var topRiskIds = localIncomingRisk.slice(0, 10).map(x=>x.source);  // top 10 local areas
        var topRiskProvIds = provincesIncomingRisk.slice(0, 5).map(x=>x.source);  // top 5 provinces
        var lines0 = graphDiv.data[0].geojson.features.filter(x=>topRiskIds.includes(x.id)).map(x => [x.centroid, selectedFeature.centroid]);
        var lines1 = graphDiv.data[1].geojson.features.filter(x=>topRiskProvIds.includes(x.id)).map(x => [x.centroid, selectedFeature.centroid]);
        var lines = lines0.concat(lines1)
        var lengths = lines.map(x => module(x[0], x[1]))  // calculate the size and distance of arrows dinamically
        var markerSize = Math.min(...lengths)/15
        var dist = Math.min(...lengths)/4
        var arrows = lines.map(x => createArrow(x[0], x[1], dist=dist, markerSize=markerSize));

        graphDiv.layout.mapbox.layers.push({
            'sourcetype': 'geojson',
            'source': {
                "type": "MultiLineString",
                "coordinates": arrows
            },
            'below': '', 'type': 'line',
            'color': colorIncomingArrows, 'opacity': 1,
            'line': {'width': 2}
        })

        Plotly.redraw(graphDiv);

        // Fill datatable information
        update_detailedLocalIncomingRisk_table(localIncomingRisk, provincesIncomingRisk, names, provincesNames);
        $('#detailedLocalIncomingRisk_div').show()
    });

}

// Outgoing
function createOutgoingRiskMap() {
    // Start creating local risk map
    local_layer_id_list[1] = ccaa_data_sublayer[province_to_ccaa[province_id_list[1]]]
    ev_list[1] = layer_to_ev[local_layer_id_list[1]]
    province_coords = province_centroids[province_id_list[1]]
    createLocalOutgoingRiskMap();
    hide_detailedLocalOutgoingRisk_table();

    // Start creating the province plots
    $('#outgoingrisktopgraph').empty();
    $('#loading-outgoingrisktopgraph').show();
    create_outgoing_risk_topgraph();

    // Risk filters
    var risk_filters = {
        "source": province_id_list[1],
        "date": date_str,
        "source_layer": "cnig_provincias",
        "target_layer": "cnig_provincias",
        "ev": "ES.covid_cpro",
    }
    Promise.all([
        fetch(staticRoute+'data/cnig_provincias.geojson'),
        fetch(apiURL+'/outgoing_risk/?where='+JSON.stringify(risk_filters)),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        // Geojson management
        var provinciasGeojson = data[0];
        var provincesNames = {};
        provinciasGeojson.features.forEach(feat => {
            provincesNames[feat.id] = feat.name;
        })
        var provinceGeojson = {
            "type": "FeatureCollection",
            "features": provinciasGeojson.features.filter(province => province.id == province_id_list[1])
        }

        // Risk management
        var outgoingRisk = data[1]['_items'];
        outgoingRisk = outgoingRisk.sort((a, b)=>a.outgoing_risk < b.outgoing_risk ? 1: -1);
        outgoingRisk = outgoingRisk.filter(a => a.target != a.source); // filter out same origin
        outgoingRisk = outgoingRisk.filter(a => a.outgoing_risk > 0.01);
        var province_total_outgoing_risk = round(outgoingRisk.reduce((a, b) => a + b.outgoing_risk, 0), 2);

        // Insert province name into hovertemplate
        $('.outgoing_province_name').text(provincesNames[province_id_list[1]])

        // Generate the fields for data
        var data = [{
            type: "choroplethmapbox",
            locations: outgoingRisk.map(x=>x.target),
            z: outgoingRisk.map(x=>x.outgoing_risk),
            customdata: outgoingRisk.map(x=>[
                provincesNames[x.target],
                round(x.trips),
                round(x.outgoing_risk, 2)]),
            geojson: provinciasGeojson,
            hovertemplate: "Province: %{customdata[0]}<extra></extra>",
            marker: {
                opacity: 0.6,
            },
            colorbar: {
                title: {
                    text: "Outgoing Risk",
                    side: "right"
                },
            },
            // colorscale: "Electric",//"YlGnBu", "Greens"
            colorscale: [['0.0', colorBase], ['1.0', colorOutgoing]],
            reversescale: false,
            hoverlabel: {align: "left"},
        }, {
            type: "choroplethmapbox",
            locations: [province_id_list[1]],
            z: [0],
            customdata: [[provincesNames[province_id_list[1]]]],
            geojson: provinceGeojson,
            hovertemplate: "Province: %{customdata[0]}<extra></extra>",
            marker: {
                opacity: 0,
            },
            showscale: false,
            hoverlabel: {align: "left"},
        }];
        
        var layout = {
            mapbox: {
                center: {
                    lon: -3.717336960173357, lat: 40.495178477814555
                },
                zoom: 5,
                style: 'carto-positron',
                layers: [{
                    "color": "black",
                    "line": {"width": 2.5},
                    "opacity": 1,
                    "sourcetype": "geojson",
                    "type": "line",
                    "source": {
                        "type": "FeatureCollection",
                        "features": [provinciasGeojson.features.find(y=>y.id == province_id_list[1])]
                    }
                },
                {                   
                    "sourcetype": "geojson",
                    "type": "fill",
                    "opacity": 0.5,
                    "color": "yellow",
                    "source": {
                        "type": "FeatureCollection",
                        "features": [provinciasGeojson.features.find(y=>y.id == province_id_list[1])]
                    }
                }
                ]
            },
            // width: 800,
            height: 500,
            margin: {"r":0,"t":0,"l":0,"b":0},
        };


        // Build arrows
        var topRiskIds = outgoingRisk.slice(0, 10).map(x=>x.target);  // the risk sources are already sorted in the database
        layout.mapbox.layers.push(
            createArrowsGeojson(provinciasGeojson.features, province_id_list[1], topRiskIds, incoming=false))

        // Create plot
        Plotly.newPlot('outgoingRiskMap', data, layout, ply_config).then(gd => {
            gd.on('plotly_click', (e) => {
                if (e.points[0].location != province_id_list[1]) {
                    province_id_list[1] = e.points[0].location;
                    createOutgoingRiskMap();
                    create_linegraph();
                }
            });

            gd.on('plotly_hover', (e) => {
                var customdata = e.points[0].customdata;
                if (e.points[0].location != province_id_list[1]) {
                    var text = customdata[0]+"<br>"+
                               "Risk coming from "+provincesNames[province_id_list[1]]+": "+customdata[2] +
                               "Trips: "+customdata[1]+"<br>"
                              
                } else {
                    var text = customdata[0]+"<br>"+
                               "Total Outgoing Risk: "+province_total_outgoing_risk+"<br>"+
                               "Population: "+round(outgoingRisk[0].source_population)+"<br>"+
                               "New Cases:"+"<br>"+
                                 "- Incidence: "+round(outgoingRisk[0].source_new_cases)+"<br>"+
                                 "- Incidence per 100k population: "+round(100000*outgoingRisk[0].source_new_cases/outgoingRisk[0].source_population, 2)+"<br>"+
                               "Accumulated Cases:"+"<br>"+
                                 "- Incidence (7 days): "+round(outgoingRisk[0].source_active_cases_7)+"<br>"+
                                 "- Incidence (14 days): "+round(outgoingRisk[0].source_active_cases_14)+"<br>"+
                                 "- Incidence (14 days) per 100k population: "+round(100000*outgoingRisk[0].source_active_cases_14/outgoingRisk[0].source_population, 2)
                }

                $('#hoverinfo_outgoingriskmap')[0].innerHTML = text;
                $('#hoverinfo_outgoingriskmap')[0].style.visibility = 'visible';
            })

            gd.on('plotly_unhover', (e) => {
                $('#hoverinfo_outgoingriskmap')[0].style.visibility = 'hidden';
            });
        });

        // Update datatable
        update_detailedOutgoingRisk_table(outgoingRisk, provincesNames)

    })
    /*.catch(function (error) {
        // Hide div in case of fail
        $('#outgoing_risk_div').hide()
        $('#local_outgoing_risk_div').hide()
        console.log('Outgoing risk map not available.')
    });*/
}

function createLocalOutgoingRiskMap() {
    if (! local_layer_id_list[1]) {
        console.log('no local data available')
        $("#localOutgoingRiskMap").text('Sorry, we do not have high resolution Covid-19 data for this CCAA.');
        return;
    } else {
        $("#localOutgoingRiskMap").empty();
    }

    // start clean, add placeholder before plotting to reserve space
    Plotly.purge("localOutgoingRiskMap");

    let area_id = null;
    var filters = {
        "type": "covid19",
        "date": date_str,
        "ev": ev_list[1],
    }

    Promise.all([
        fetch(staticRoute+'data/hires_province_'+province_id_list[1]+'.geojson'),
        fetch(staticRoute+'data/cnig_provincias.geojson'),
        fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters)+"&max_results=500"),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        var geojson = data[0];
        var provincesGeojson = data[1];
        var covidData = data[2]['_items'];
        var covidDataIndex = {}; covidData.forEach(d => {covidDataIndex[d.id] = d})

        var data = [
            {
                type: "choroplethmapbox",
                locations: geojson.features.map(x=>x.id),
                z: geojson.features.map(x=>(covidDataIndex[x.id] || {}).active_cases_14 || 0),
                customdata: geojson.features.map(x=>[x.id, x.name]),
                geojson: geojson,
                hovertemplate: "Name: %{customdata[1]},<br>Area id: %{customdata[0]},<br>Incidence (14 days): %{z}<extra></extra>",
                marker: {
                    opacity: 0.6,
                },
                colorbar: {title: {text: "Incidence (14 days)"}},
                // colorscale: 'Reds',
                colorscale: [['0.0', colorBase], ['1.0', colorIncidence]],
                hoverlabel: {align: "left"},
            },
            {
                type: "choroplethmapbox",
                locations: [],
                // locations: provincesGeojson.features.map(x=>x.id),
                z: [],
                z: provincesGeojson.features.map(x=>1),
                // customdata: [],
                geojson: provincesGeojson,
                // hovertemplate: "Name: %{customdata[1]},<br>Area id: %{customdata[0]},<br>Daily incidence: %{z}<extra></extra>",
                marker: {
                    opacity: 0.6,
                },
                colorbar: {title: {text: "Incidence (14 days)"}},
                // colorscale: 'Reds',
                colorscale: [['0.0', colorBase], ['1.0', colorIncidence]],
                hoverlabel: {align: "left"},
            },
        ];

        var layout = {
            mapbox: {
                center: {
                    lon: province_coords[0], lat: province_coords[1]
                },
                zoom: 7,
                style: 'carto-positron',
            },
            height: 500,
            margin: {"r":0,"t":0,"l":0,"b":0},
        };

        Plotly.plot('localOutgoingRiskMap', data, layout, ply_config).then(gd => {
            gd.on('plotly_click', (e) => {

                if (e.points[0].curveNumber == 1) {
                    // clicked on a province
                    // alert("You have clicked on a province (if you wish, you can select a different province in the upper map)")
                    if (confirm("You have clicked on a different province. Do you want to continue?")) {
                        province_id_list[1] = e.points[0].location;
                        createOutgoingRiskMap();
                    }
                    return;
                }

                if (area_id != e.points[0].location) {
                    // DIFFERENT AREA THAN THE PREVIOUSLY SELECTED
                    area_id = e.points[0].location;
                    selectAreaLocalOutgoingRiskMap(area_id);
                } else {
                    // SAME AREA THAN THE PREVIOUSLY SELECTED
                    hide_detailedLocalOutgoingRisk_table();
                    createLocalOutgoingRiskMap();
                }
            });
        });

    })
//    .catch(function (error) {
//        // Hide div in case of fail
//        $('#local_outgoing_risk_div').hide()
//        console.log('Local outgoing risk map not available.')
//    });
}

function selectAreaLocalOutgoingRiskMap(area_id) {
    var filters = {
        "source": area_id,
        "date": date_str,
        "source_layer": local_layer_id_list[1],
        "target_layer": local_layer_id_list[1],
        "ev": ev_list[1],
    }
    var filters2 = {
        "source": area_id,
        "date": date_str,
        "source_layer": local_layer_id_list[1],
        "target_layer": 'cnig_provincias',
        "ev": ev_list[1],
    }
    Promise.all([
        fetch(apiURL+'/outgoing_risk/?where='+JSON.stringify(filters)),
        fetch(apiURL+'/outgoing_risk/?where='+JSON.stringify(filters2)),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        if ('error' in data[0]) {
            alert(JSON.stringify(data[0])); return;
        }
        if ('error' in data[1]) {
            alert(JSON.stringify(data[1])); return;
        }
        if (!Array.isArray(data[0]._items) || !data[0]._items.length) {
            alert('Sorry, there is no data for the selected area.');
            return;
        }
        var outgoingRisk = data[0]._items.filter(a=>a.outgoing_risk > 0.01)
        var provincesOutRisk = data[1]._items;
        outgoingRisk = outgoingRisk.sort((a, b)=>a.outgoing_risk < b.outgoing_risk ? 1: -1)

        provincesOutRisk = provincesOutRisk.filter(a=>a.outgoing_risk > 0.01)
        provincesOutRisk = provincesOutRisk.sort((a, b)=>a.outgoing_risk < b.outgoing_risk ? 1: -1)
        provincesOutRisk = provincesOutRisk.filter(a=>a.target != province_id_list[1])

        var graphDiv = document.getElementById('localOutgoingRiskMap');
        var features = graphDiv.data[0].geojson.features;
        var selectedFeature = features.find(y=>y.id == area_id);
        var names = {}; features.forEach(feat => {names[feat.id] = feat.name;})
        var provincesNames = {}; graphDiv.data[1].geojson.features.forEach(feat => {provincesNames[feat.id] = feat.name;})

        graphDiv.data[0].locations = outgoingRisk.map(x=>x.target);
        graphDiv.data[0].customdata = outgoingRisk.map(x=>[names[x.target], round(x.trips), round(x.outgoing_risk, 2)]);
        graphDiv.data[0].z = outgoingRisk.map(x=>x.outgoing_risk);
        graphDiv.data[0].hovertemplate = "Name: %{customdata[0]}<br>Trips: %{customdata[1]}<br>Risk: %{customdata[2]}<extra></extra>";
        graphDiv.data[0].colorscale = [['0.0', colorBase], ['1.0', colorOutgoing]];
        graphDiv.data[0].colorbar = {title: {text: "Outgoing Risk"}};

        // update provinces risk trace
        graphDiv.data[1].locations = provincesOutRisk.map(x=>x.target);
        graphDiv.data[1].customdata = provincesOutRisk.map(x=>[provincesNames[x.target], round(x.trips), round(x.outgoing_risk, 2)]);
        graphDiv.data[1].z = provincesOutRisk.map(x=>x.outgoing_risk);
        graphDiv.data[1].hovertemplate = "Name: %{customdata[0]}<br>Trips: %{customdata[1]}<br>Risk: %{customdata[2]}<extra></extra>";
        graphDiv.data[1].colorscale = [['0.0', colorBase], ['1.0', colorOutgoing]];
        graphDiv.data[1].colorbar = {title: {text: "Outgoing Risk"}};
        graphDiv.data[1].showscale = false;
        graphDiv.data[1].zmax = Math.max(...graphDiv.data[0].z)
        graphDiv.data[1].zmin = 0

        // Adding the same area_id that is selected
        graphDiv.data[2] = {
            type: "choroplethmapbox",
            locations: [area_id],
            geojson: {
                "type": "FeatureCollection",
                "features": graphDiv.data[0].geojson.features.filter(area => area.id == area_id)
            },
            z: [0],
            customdata: [[names[area_id], area_id]],
            hovertemplate: "Name: %{customdata[0]}<br>Area id: %{customdata[1]}<extra></extra>",
            marker: {
                opacity: 0,
            },
            showscale: false,
            hoverlabel: {align: "left"},
        }

        graphDiv.layout.mapbox.layers = [
            {
                "color": "black",
                "line": {"width": 2.5},
                "opacity": 1,
                "sourcetype": "geojson",
                "type": "line",
                "source": {
                    "type": "FeatureCollection",
                    "features": [selectedFeature]
                }
            },
            {
                "sourcetype": "geojson",
                "type": "fill",
                "opacity": 0.5,
                "color": "yellow",
                "source": {
                    "type": "FeatureCollection",
                    "features": [selectedFeature]
                }
            }
        ];

        // Build arrows
        var topRiskIds = outgoingRisk.slice(0, 10).map(x=>x.target);  // top 10 local areas
        var topRiskProvIds = provincesOutRisk.slice(0, 5).map(x=>x.target);  // top 5 provinces
        var lines0 = graphDiv.data[0].geojson.features.filter(x=>topRiskIds.includes(x.id)).map(x => [selectedFeature.centroid, x.centroid]);
        var lines1 = graphDiv.data[1].geojson.features.filter(x=>topRiskProvIds.includes(x.id)).map(x => [selectedFeature.centroid, x.centroid]);
        var lines = lines0.concat(lines1)
        var lengths = lines.map(x => module(x[0], x[1]))  // calculate the size and distance of arrows dinamically
        var markerSize = Math.min(...lengths)/15
        var dist = Math.min(...lengths)/8
        var arrows = lines.map(x => createOutgoingArrow(x[0], x[1], dist=dist, markerSize=markerSize));

        graphDiv.layout.mapbox.layers.push({
            'sourcetype': 'geojson',
            'source': {
                "type": "MultiLineString",
                "coordinates": arrows
            },
            'below': '', 'type': 'line',
            'color': colorOutgoingArrows, 'opacity': 1,
            'line': {'width': 2}
        })

        Plotly.redraw(graphDiv);

        // Fill datatable information
        update_detailedLocalOutgoingRisk_table(outgoingRisk, provincesOutRisk, names, provincesNames, area_id);
        $('#detailedLocalOutgoingRisk_div').show()
    });
}


// =====================================  DATATABLE MANAGEMENT  =====================================
function update_datatable(table, data) {
    var datatable = $(table).DataTable();
    datatable.clear();
    datatable.rows.add(data);
    datatable.columns.adjust();
    datatable.draw();
}

// Incoming risk
function create_detailedIncomingRisk_table() {
    column_names = ['province_name', 'incoming_risk', 'population', 'trips', 'active_cases', 'active_per_100000']  // , 'tendency']
    columns_rename = {
        'province_name': i18next.t('datatable.province_name'),
        'incoming_risk': i18next.t('datatable.incoming_risk'),
        'population': i18next.t('datatable.population'),
        'active_cases': i18next.t('datatable.active_cases'),
        'active_per_100000': i18next.t('datatable.active_per_100000'),
        'trips': i18next.t('datatable.trips'),
    }

    // Create the table with the data info, the column headers, and the default order (sorted_by)
    var table = $('#detailedIncomingRisk_table').DataTable({
        columns: column_names.map(function(column) { return {title: columns_rename[column]} }),
        order: [[1, 'desc']],
        columnDefs: [
            { "width": "15%", "targets": 0 },
            { "width": "15%", "targets": 1 },
            { "className": "dt-center", "targets": "_all" },
        ]
    });
}

function update_detailedIncomingRisk_table(data, provinces_names) {
    // Update the datatable
    var dataSet = [];
    data.forEach(detailed_risk => {
        dataSet.push([
            provinces_names[detailed_risk['source']],
            detailed_risk['incoming_risk'].toFixed(2),
            parseInt(detailed_risk['source_population']),
            parseInt(detailed_risk['trips']),
            parseInt(detailed_risk['source_active_cases_14']),
            parseInt(100000 * detailed_risk['source_active_cases_14'] / detailed_risk['source_population']),
        ]);
    })
    update_datatable('#detailedIncomingRisk_table', dataSet);
}

function create_detailedLocalIncomingRisk_table() {
    column_names = ['zone', 'incoming_risk', 'population', 'trips', 'active_cases', 'active_per_100000']
    columns_rename = {
        'zone': i18next.t('datatable.zone'),
        'incoming_risk': i18next.t('datatable.incoming_risk'),
        'population': i18next.t('datatable.population'),
        'trips': i18next.t('datatable.trips'),
        'active_cases': i18next.t('datatable.active_cases'),
        'active_per_100000': i18next.t('datatable.active_per_100000'),
    }
    var table = $('#detailedLocalIncomingRisk_table').DataTable({
        columns: column_names.map(function(column) { return {title: columns_rename[column]} }),
        order: [[1, 'desc']],
        columnDefs: [
                { "width": "20%", "targets": 0 },
                { "width": "15%", "targets": 1 },
                { "className": "dt-center", "targets": "_all" },
            ]
    });
    $('#detailedLocalIncomingRisk_div').hide();
}

function update_detailedLocalIncomingRisk_table(localIncomingRisk, provincesIncomingRisk, names, provincesNames) {
    // Build table with the information (first from local zones, after from provinces)
    $('#detailedLocalIncomingRisk_div').show()
    var dataSet = [];
    localIncomingRisk.forEach(detailed_risk => {
        if (!(detailed_risk['source'] in names)) {
            return //skip
        }
        dataSet.push([
            names[detailed_risk['source']],
            detailed_risk['incoming_risk'].toFixed(2),
            parseInt(detailed_risk['source_population']),
            parseInt(detailed_risk['trips']),
            parseInt(detailed_risk['source_active_cases_14']),
            parseInt(100000 * detailed_risk['source_active_cases_14']/detailed_risk['source_population']),
        ]);
    })
    provincesIncomingRisk.forEach(detailed_risk => {
        dataSet.push([
            provincesNames[detailed_risk['source']] + ' (' + i18next.t('datatable.province_name') + ')',
            detailed_risk['incoming_risk'].toFixed(2),
            parseInt(detailed_risk['source_population']),
            parseInt(detailed_risk['trips']),
            parseInt(detailed_risk['source_active_cases_14']),
            parseInt(100000 * detailed_risk['source_active_cases_14']/detailed_risk['source_population']),
        ]);
    })
    update_datatable('#detailedLocalIncomingRisk_table', dataSet)
}

function hide_detailedLocalIncomingRisk_table() {
    $('#detailedLocalIncomingRisk_div').hide();
}


// Outgoing risk
function create_detailedOutgoingRisk_table() {
    column_names = ['province_name', 'outgoing_risk', 'trips']
    columns_rename = {
        'province_name': i18next.t('datatable.province_name'),
        'outgoing_risk': i18next.t('datatable.outgoing_risk'),
        'trips': i18next.t('datatable.trips'),
    }

    // Create the table with the data info, the column headers, and the default order (sorted_by)
    var table = $('#detailedOutgoingRisk_table').DataTable({
        columns: column_names.map(function(column) { return {title: columns_rename[column]} }),
        order: [[1, 'desc']],
        columnDefs: [
            { "width": "40%", "targets": 0 },
            { "className": "dt-center", "targets": "_all" },
        ]
    });
}

function update_detailedOutgoingRisk_table(data, provinces_names) {
    var dataSet = [];
    data.forEach(detailed_risk => {
        // Fill the table row
        dataSet.push([
            provinces_names[detailed_risk['target']],
            detailed_risk['outgoing_risk'].toFixed(2),
            parseInt(detailed_risk['trips']),
        ]);
    })
    update_datatable('#detailedOutgoingRisk_table', dataSet);
}

function create_detailedLocalOutgoingRisk_table() {
    column_names = ['zone', 'outgoing_risk', 'trips']
    columns_rename = {
        'zone': i18next.t('datatable.zone'),
        'outgoing_risk': i18next.t('datatable.outgoing_risk'),
        'trips': i18next.t('datatable.trips'),
    }
    var table = $('#detailedLocalOutgoingRisk_table').DataTable({
        columns: column_names.map(function(column) { return {title: columns_rename[column]} }),
        order: [[1, 'desc']],
        columnDefs: [
            { "width": "40%", "targets": 0 },
            { "className": "dt-center", "targets": "_all" },
        ]
    });
    $('#detailedLocalOutgoingRisk_div').hide();
}

function update_detailedLocalOutgoingRisk_table(localOutgoingRisk, provincesOutgoingRisk, names, provincesNames, area_id) {
    // SHow the div that contains the table
    $('#detailedLocalOutgoingRisk_div').show()

    // Iter the items in order to get the information and populate the lists for the table
    var dataSet = [];

    // Incoming risk from local zones
    localOutgoingRisk.forEach(detailed_risk => {
        if (!(detailed_risk['target'] in names)) {
            return //skip
        }
        dataSet.push([
            names[detailed_risk['target']],
            detailed_risk['outgoing_risk'].toFixed(2),
            parseInt(detailed_risk['trips']),
        ]);
    })

    // Incoming risk from provinces
    provincesOutgoingRisk.forEach(detailed_risk => {
        dataSet.push([
            provincesNames[detailed_risk['target']] + ' (' + i18next.t('datatable.province_name') + ')',
            detailed_risk['outgoing_risk'].toFixed(2),
            parseInt(detailed_risk['trips']),
        ]);
    })
    update_datatable('#detailedLocalOutgoingRisk_table', dataSet)
}

function hide_detailedLocalOutgoingRisk_table() {
    $('#detailedLocalOutgoingRisk_div').hide();
}


// =====================================  PLOT MANAGEMENT  =====================================
// Linegraph: Incoming + outgoing risk + incidence vs time
function create_linegraph() {
    // PLOT INCOMING/OUTGOING RISK & CASES vs TIME

    $('#risklinegraph').empty();
    $('#loading-risklinegraph').show();

    // Risk Request
    var incoming_risk_filters = {
        "target": province_id_list[0],
        "source_layer": "cnig_provincias",
        "target_layer": "cnig_provincias",
        "ev": "ES.covid_cpro",
        "total": true,
    };
    var incoming_risk_url = apiURL + '/incoming_risk_history/?where=' + JSON.stringify(incoming_risk_filters);
    var outgoing_risk_filters = {
        "source": province_id_list[1],
        "source_layer": "cnig_provincias",
        "target_layer": "cnig_provincias",
        "ev": "ES.covid_cpro",
        "total": true,
    };
    var outgoing_risk_url = apiURL + '/outgoing_risk_history/?where=' + JSON.stringify(outgoing_risk_filters);

    // Cases Request
//    let cases_filters = {type: "covid19", ev: "ES.covid_cpro", "id": province_id};
//    let cases_url = apiURL + '/layers.data.consolidated/?where=' + JSON.stringify(cases_filters)
//                                                                 + '&projection={"detailed_risk": 0}&max_results=500';
    Promise.all([
        fetch(incoming_risk_url),
        fetch(outgoing_risk_url),
//        fetch(cases_url),
        fetch(staticRoute+'json/provinces.json'),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        let dates = null;
        let provinces_id_to_name = data[2];

        // Processing Incoming Risk data and building arrays
        var incoming_risk_data_set = data[0]._items;
        dates = incoming_risk_data_set.map(function(doc) {return doc.date;});
        let incoming_risk_data = incoming_risk_data_set.map(function(doc) {return doc.incoming_risk;});

        // Processing Outgoing Risk data and building arrays
        var outgoing_risk_data_set = data[1]._items;
        dates = outgoing_risk_data_set.map(function(doc) {return doc.date;});
        let outgoing_risk_data = outgoing_risk_data_set.map(function(doc) {return doc.outgoing_risk;});

        // Processing Daily cases data and building arrays
//        let new_cases = [];
//        var cases_data_set = data[2]._items;
//        dates.forEach(date => {
//            new_cases.push(cases_data_set.filter(doc => doc.date == date)[0].new_cases);
//        })

        // Define plot data (traces)
        var plt_data = [];
        for (var i=0; i<3; i++) {
            plt_data.push(
                {
                    name: 'Incoming Risk',
                    x: dates,
                    y: i!=2? calculate_smoothy_data(incoming_risk_data, i==0? 7: 14): incoming_risk_data,
                    mode: 'lines',
                    type: 'scatter',
                    line: {color: 'red', width: 1.5 },
                    visible: i==0,
                    hovertemplate: provinces_id_to_name[province_id_list[0]] +
                                   "<br>%{x|%Y-%m-%d (%A)}" +
                                   "<br>Incoming risk: %{y}",
                },
                {
                    name: 'Outgoing Risk',
                    x: dates,
                    y: i!=2? calculate_smoothy_data(outgoing_risk_data, i==0? 7: 14): outgoing_risk_data,
                    mode: 'lines',
                    type: 'scatter',
                    line: {color: 'purple', width: 1.5 },
                    visible: i==0,
                    hovertemplate: provinces_id_to_name[province_id_list[1]] +
                                   "<br>%{x|%Y-%m-%d (%A)}" +
                                   "<br>Outgoing risk: %{y}",
                },
//                {
//                    name: 'Incidence',
//                    x: dates,
//                    y: i!=2? calculate_smoothy_data(new_cases, i==0? 7: 14): new_cases,
//                    mode: 'lines',
//                    yaxis: 'y2',
//                    type: 'scatter',
//                    line: {color: 'blue', width: 1.5},
//                    visible: i==0,
//                    hovertemplate: province_name+"<br>%{x|%Y-%m-%d (%A)}<br>Covid-19 incidence: %{y}",
//                }
            );
        }

        // Define the plot layout
        layout = {
            template: {
                layout: {
                    hovermode: "closest",
                    paper_bgcolor: "white",
                    plot_bgcolor: "#E5ECF6",
                    xaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    yaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    colorway: ["#636efa", "#EF553B", "#00cc96", "#ab63fa", "#FFA15A", "#19d3f3"],
                }
            },
            yaxis: {anchor: 'x', domain: [0.0, 1.0], fixedrange: true, title: {text: 'Mobility Associated Risk'}},
            yaxis2: {anchor: 'x', overlaying: 'y', side: 'right', title: {text: 'Daily cases'}},
            legend: {orientation: 'h', x: 1, xanchor: 'right', y: 1.02, yanchor: 'bottom'},
            margin: {r: 55, t: 0, l: 0, b: 0},
            height: 300,
            updatemenus: [{
                y: 1,
                yanchor: 'top',
                buttons: [{
                    method: 'restyle',
                    args: ['visible', [true, true, false, false, false, false]],
                    label: i18next.t('plots.cumulative_incidence_7_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [false, false, true, true, false, false]],
                    label: i18next.t('plots.cumulative_incidence_14_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [false, false, false, false, true, true]],
                    label: i18next.t('plots.daily_incidence')
                }],
                direction: "right",
                x: 0.01,
                xanchor: "left",
                yanchor: "top",
            }],
        };

        Plotly.newPlot('risklinegraph', plt_data, layout, {displayModeBar: false});
        $('#loading-risklinegraph').hide();
    }).catch(function (error) {
        // Hide div in case of fail
        $('#risk_linegraph_div').hide()
        console.log('Line graph not available.')
    });
}

// Top graph: Detailed risk vs time
function create_incoming_risk_topgraph() {
    // PLOT INCOMING RISK BY ZONES vs TIME

    let filters = {layer: "cnig_provincias", id: province_id_list[0]};
    var risk_filters = {
        "target": province_id_list[0],
        "source_layer": "cnig_provincias",
        "target_layer": "cnig_provincias",
        "ev": "ES.covid_cpro",
    };
    var url = apiURL + '/incoming_risk_history/?where=' + JSON.stringify(risk_filters);

    Promise.all([
        fetch(url),
        fetch(staticRoute+'json/provinces.json'),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        let dates = [];
        let new_cases = [];
        let risk_data_by_provinces = {};

        var evolution_data = data[0]._items;
        var province_id_to_name = data[1];

        // Loop for the documents and store the risk separated by province:
        // risk_data_by_provinces = {'Barcelona': [1, 2, 3]}
        // where [1, 2, 3] corresponds to the risk at the dates list `dates`.
        evolution_data.sort((a, b)=>a.date < b.date ? 1: -1)
        var last_date = null;
        evolution_data.forEach(doc => {
            var province_name = province_id_to_name[doc.source];
            if (province_name in risk_data_by_provinces) {
                risk_data_by_provinces[province_name].push(doc.incoming_risk);
            } else {
                // Only first iteration
                risk_data_by_provinces[province_name] = [doc.incoming_risk];
            };

            // Store the dates (do not save the same date multiple times
            if (last_date != doc.date) {
                dates.push(doc.date);
                last_date = doc.date;
            }
        });

        // Define plot data (traces)
        var plt_data = [];
        var n_provinces = Object.values(risk_data_by_provinces).length
        for (var i=0; i<3; i++) {
            for (var province_name in risk_data_by_provinces) {
                var risk_data = risk_data_by_provinces[province_name];
                plt_data.push(
                    {
                        name: province_name,
                        x: dates,
                        y: i!=2? calculate_smoothy_data(risk_data, i==0? 7: 14): risk_data,
                        mode: 'lines',
                        type: 'scatter',
                        line: {width: 1.5 },
                        visible: i==0,
                        hovertemplate: "%{x|%Y-%m-%d (%A)}<br>Incoming risk: %{y}",
                    },
                );
            }
        }

        // Define the plot layout
        var button_option_true = new Array(n_provinces).fill(true)
        var button_option_false = new Array(n_provinces).fill(false)

        layout = {
            template: {
                layout: {
                    hovermode: "closest",
                    paper_bgcolor: "white",
                    plot_bgcolor: "#E5ECF6",
                    xaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    yaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    colorway: ["#636efa", "#EF553B", "#00cc96", "#ab63fa", "#FFA15A", "#19d3f3"],
                }
            },
            yaxis: {anchor: 'x', domain: [0.0, 1.0], fixedrange: true, title: {text: 'Incoming Risk'}},
            yaxis2: {anchor: 'x', overlaying: 'y', side: 'right', title: {text: 'Daily cases'}},
            margin: {r: 55, t: 0, l: 0, b: 0},
            height: 300,
            updatemenus: [{
                y: 1,
                yanchor: 'top',
                buttons: [{
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_true, button_option_false, button_option_false)],
                    label: i18next.t('plots.cumulative_incidence_7_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_false, button_option_true, button_option_false)],
                    label: i18next.t('plots.cumulative_incidence_14_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_false, button_option_false, button_option_true)],
                    label: i18next.t('plots.daily_incidence')
                }],
                direction: "right",
                x: 0.01,
                xanchor: "left",
                yanchor: "top",
                legend: {traceorder: 'normal'},
            }],
        };

        Plotly.newPlot('incomingrisktopgraph', plt_data, layout, {displayModeBar: false});
        $('#loading-incomingrisktopgraph').hide();
    }).catch(function (error) {
        // Hide div in case of fail
        $('#incoming_risk_topgraph_div').hide()
        console.log('Incoming risk topgraph not available.')
    });
}


function create_outgoing_risk_topgraph() {
    // PLOT OUTGOING RISK BY ZONES vs TIME

    let filters = {layer: "cnig_provincias", id: province_id_list[1]};
    var risk_filters = {
        "source": province_id_list[1],
        "source_layer": "cnig_provincias",
        "target_layer": "cnig_provincias",
        "ev": "ES.covid_cpro",
    };
    var url = apiURL + '/outgoing_risk_history/?where=' + JSON.stringify(risk_filters);

    Promise.all([
        fetch(url),
        fetch(staticRoute+'json/provinces.json'),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        let dates = [];
        let new_cases = [];
        let risk_data_by_provinces = {};

        var evolution_data = data[0]._items;
        var province_id_to_name = data[1];

        // Loop for the documents and store the risk separated by province:
        // risk_data_by_provinces = {'Barcelona': [1, 2, 3]}
        // where [1, 2, 3] corresponds to the risk at the dates list `dates`.
        evolution_data.sort((a, b)=>a.date < b.date ? 1: -1)
        var last_date = null;
        evolution_data.forEach(doc => {
            var province_name = province_id_to_name[doc.target];
            if (province_name in risk_data_by_provinces) {
                risk_data_by_provinces[province_name].push(doc.outgoing_risk);
            } else {
                // Only first iteration
                risk_data_by_provinces[province_name] = [doc.outgoing_risk];
            };

            // Store the dates (do not save the same date multiple times
            if (last_date != doc.date) {
                dates.push(doc.date);
                last_date = doc.date;
            }
        });

        // Define plot data (traces)
        var plt_data = [];
        var n_provinces = Object.values(risk_data_by_provinces).length
        for (var i=0; i<3; i++) {
            for (var province_name in risk_data_by_provinces) {
                var risk_data = risk_data_by_provinces[province_name];
                plt_data.push(
                    {
                        name: province_name,
                        x: dates,
                        y: i!=2? calculate_smoothy_data(risk_data, i==0? 7: 14): risk_data,
                        mode: 'lines',
                        type: 'scatter',
                        line: {width: 1.5 },
                        visible: i==0,
                        hovertemplate: "%{x|%Y-%m-%d (%A)}<br>Outgoing risk: %{y}",
                    },
                );
            }
        }

        // Define the plot layout
        var button_option_true = new Array(n_provinces).fill(true)
        var button_option_false = new Array(n_provinces).fill(false)

        layout = {
            template: {
                layout: {
                    hovermode: "closest",
                    paper_bgcolor: "white",
                    plot_bgcolor: "#E5ECF6",
                    xaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    yaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    colorway: ["#636efa", "#EF553B", "#00cc96", "#ab63fa", "#FFA15A", "#19d3f3"],
                }
            },
            yaxis: {anchor: 'x', domain: [0.0, 1.0], fixedrange: true, title: {text: 'Outgoing Risk'}},
            yaxis2: {anchor: 'x', overlaying: 'y', side: 'right', title: {text: 'Daily cases'}},
            margin: {r: 55, t: 0, l: 0, b: 0},
            height: 300,
            updatemenus: [{
                y: 1,
                yanchor: 'top',
                buttons: [{
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_true, button_option_false, button_option_false)],
                    label: i18next.t('plots.cumulative_incidence_7_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_false, button_option_true, button_option_false)],
                    label: i18next.t('plots.cumulative_incidence_14_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(button_option_false, button_option_false, button_option_true)],
                    label: i18next.t('plots.daily_incidence')
                }],
                direction: "right",
                x: 0.01,
                xanchor: "left",
                yanchor: "top",
                legend: {traceorder: 'normal'},
            }],
        };

        Plotly.newPlot('outgoingrisktopgraph', plt_data, layout, {displayModeBar: false});
        $('#loading-outgoingrisktopgraph').hide();
    }).catch(function (error) {
        // Hide div in case of fail
        $('#outgoing_risk_topgraph_div').hide()
        console.log('Outgoing risk topgraph not available.')
    });
}


// =====================================  DATEPICKER  =====================================
function onChangeDate(new_date_str) {
    date_str = new_date_str;
    $('.selected-date').text(date_str);

    if ($('.nav-tabs .active')[0].id == 'incoming-tab') {
        createIncomingRiskMap();
        $('#outgoingRiskMap').empty().removeClass( "js-plotly-plot" );
    } else {
        createOutgoingRiskMap();
        $('#incomingRiskMap').empty().removeClass( "js-plotly-plot" );
    }
}

configCalendar(min_date, max_date, onChangeDate);


// =====================================  INITIALIZATION  =====================================

i18next.on('initialized', function(options) {
    // Incoming
    createIncomingRiskMap();

    create_detailedIncomingRisk_table();

    create_detailedLocalIncomingRisk_table();

    $('#incomingrisktopgraph').empty();
    $('#loading-incomingrisktopgraph').show();
    create_incoming_risk_topgraph();

    // Outgoing
    create_detailedOutgoingRisk_table();

    create_detailedLocalOutgoingRisk_table();

    create_linegraph();
})