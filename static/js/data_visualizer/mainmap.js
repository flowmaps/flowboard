

function initMap(vueobj) {
    var options = {boxSelect: true, boxZoom: false, attributionControl: false}
    var map = L.map(vueobj.mapid, options).setView([40.495178477814555, -3.717336960173357], 6);
    // L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png').addTo(map);
    L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png').addTo(map);
    map.zoomControl.setPosition('bottomright')

    map.on('boxselectend', function(e) {
        var selectedIds = vueobj.geojson.features.filter(feat=>e.boxSelectBounds.contains([feat.centroid[1], feat.centroid[0]])).map(feat=>feat.id)
        console.log('selected ids:', selectedIds)
        vueobj.selectedAreasIds = selectedIds
    })


    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        this.update();
        return this._div;
    };

    // method that we will use to update the control based on feature properties passed
    info.update = function (text) {
        this._div.innerHTML = text || "";
    };

    info.setPosition('bottomleft')

    info.addTo(map);

    map.info = info

    return map
}


function cleanMap(map) {
    map.eachLayer(function (layer) {
        if(!(layer instanceof L.TileLayer)) {
            map.removeLayer(layer);
        }
    })
}


function cleanMapBorderAreas(map) {
    map.eachLayer(function (layer) {
        if (layer.type == 'borders') {
            map.removeLayer(layer);
        }
    })
}


function handleClickOnArea(e, vueobj, areaid) {
    console.log('clicked on:', areaid)

    if (!vueobj.selectedAreasIds || vueobj.selectedAreasIds.length == 0) {
        vueobj.selectedAreasIds = [areaid]
    } else if (e.originalEvent.ctrlKey || e.originalEvent.shiftKey) {
        if (vueobj.selectedAreasIds.includes(areaid)) {
            vueobj.selectedAreasIds = vueobj.selectedAreasIds.filter(x=>x!=areaid)
        } else {
            vueobj.selectedAreasIds.push(areaid)
        }
    } else {
        vueobj.selectedAreasIds = [areaid]
    }
}


function addBorderToSelectedAreas(vueobj, applyFn) {
    if (!vueobj.selectedAreas) {
        return
    }
    
    var map = vueobj.map

    cleanMapBorderAreas(map)

    function style(feature) {
        return {
            fillColor: 'black',
            weight: 2,
            opacity: 1,
            color: 'black',
            fillOpacity: 0.1
        };
    }

    var borders = L.geoJson(vueobj.selectedAreas, {style: style})
    borders.on('click', e=>{
        handleClickOnArea(e, vueobj, e.layer.feature.id)
    })
    applyFn(borders)
    borders.type = 'borders'
    borders.addTo(map);
}


var mapControllerPlain = {
    paintMap: async function(vueobj) {
        if (!vueobj.map) {
            vueobj.map = initMap(vueobj)
        }

        var map = vueobj.map

        cleanMap(map)

        function style(feature) {
            return {
                fillColor: "#8ea0f6",
                weight: 1,
                opacity: 1,
                color: '#53566b',
                // dashArray: '3',
                fillOpacity: 0.75
            };
        }

        var polygons = L.geoJson(vueobj.geojson.features, {style: style})
        polygons.on('click', e=>{
            handleClickOnArea(e, vueobj, e.layer.feature.id)
        }).on('mouseover', e=>{
            map.info.update(e.layer.feature.name);
        }).on('mouseout', e=>{
            map.info.update("");
        })
        polygons.addTo(map);
    },
    onSelectAreas: function(vueobj) {
        addBorderToSelectedAreas(vueobj, borders=>{
            borders.on('mouseover', e=>{
                vueobj.map.info.update(e.layer.feature.name);
            }).on('mouseout', e=>{
                vueobj.map.info.update("");
            })
        });
    },
}


var mapControllerColor = {
    paintMap: async function(vueobj) {
        if (!vueobj.map) {
            vueobj.map = initMap(vueobj)
        }

        var map = vueobj.map
        let loc = vueobj.dataSource.loc || 'id'
        let field = vueobj.dataSource.field

        // download data
        if (vueobj.dataSource.data) {
            var values = await vueobj.dataSource.data(vueobj)
        } else {
            var filters = Object.assign({}, vueobj.dataSource.filters) // copy
            if (vueobj.dataSource.daily) {
                filters['date'] = vueobj.date
            }
            let url = buildApiUrl(vueobj.dataSource.collection, filters, 500, {"d": 0, "c": 0})
            let data = await fetchAllPages(url)
            var values = {}; data.forEach(d=>values[d[loc]] = d[field])
        }

        let arr = Object.values(values);
        let min = Math.min(...arr);
        let max = Math.max(...arr);
        vueobj.geojson.features.forEach(x=>{x.z = values[x.id]}) // enrich geojson features

        // clean map
        cleanMap(map)

        // paint map with colors
        var colorscale = vueobj.dataSource.colorscale || ['#fffafa', '#8f000b']
        var scale = chroma.scale(colorscale).domain([min, max]);
        
        function style(feature) {
            return {
                fillColor: scale(feature.z).hex(),
                weight: 1,
                opacity: 1,
                color: '#53566b',
                // dashArray: '3',
                fillOpacity: 0.75
            };
        }

        var polygons = L.geoJson(vueobj.geojson.features, {style: style})
        polygons.on('click', e=>{
            handleClickOnArea(e, vueobj, e.layer.feature.id)
        }).on('mouseover', e=>{
            let value = e.layer.feature.z ? e.layer.feature.z.toFixed(1) : '?'
            map.info.update(e.layer.feature.name+": "+value+" ("+vueobj.dataSource.name+" "+vueobj.date+")");
        }).on('mouseout', e=>{
            map.info.update("");
        })
        polygons.addTo(map);

        // add border to selected areas
        addBorderToSelectedAreas(vueobj, borders=>{
            borders.on('mouseover', e=>{
                vueobj.map.info.update(e.layer.feature.name+": "+e.layer.feature.z.toFixed(1)+" ("+vueobj.dataSource.name+" "+vueobj.date+")");
            }).on('mouseout', e=>{
                vueobj.map.info.update("");
            })
        });
    },
    onSelectAreas: function(vueobj) {
        addBorderToSelectedAreas(vueobj, borders=>{
            borders.on('mouseover', e=>{
                vueobj.map.info.update(e.layer.feature.name+": "+e.layer.feature.z.toFixed(1)+" ("+vueobj.dataSource.name+" "+vueobj.date+")");
            }).on('mouseout', e=>{
                vueobj.map.info.update("");
            })
        });
    },
}



var mapControllerArrows = {
    paintMap: async function(vueobj) {
        if (!vueobj.map) {
            vueobj.map = initMap(vueobj)
        }

        var map = vueobj.map
        let loc = vueobj.dataSource.loc || 'id'
        let field = vueobj.dataSource.field

        if (!vueobj.selectedAreasIds || vueobj.selectedAreasIds.length == 0) {
            vueobj.selectedAreasIds = [vueobj.dataSource.arrows_default_area]
            // return mapControllerPlain.paintMap(vueobj)
        }

        // fetch data
        let values = await vueobj.dataSource.data(vueobj)
        let arr = Object.values(values);
        let min = Math.min(...arr);
        let max = Math.max(...arr);
        vueobj.geojson.features.forEach(x=>{x.z = values[x.id] || 0}) // enrich geojson features

        // clean map
        cleanMap(map)

        // paint map with colors
        var colorscale = vueobj.dataSource.colorscale || ['#e7e4e2', '#4b61c7']
        var scale = chroma.scale(colorscale).domain([min, max]);
        
        function style(feature) {
            return {
                fillColor: feature.z ? scale(feature.z).hex() : '#e7e4e2',
                weight: 1,
                opacity: 1,
                color: '#53566b',
                // dashArray: '3',
                fillOpacity: 0.75
            };
        }

        var polygons = L.geoJson(vueobj.geojson.features, {style: style})
        polygons.on('click', e=>{
            handleClickOnArea(e, vueobj, e.layer.feature.id)
        }).on('mouseover', e=>{
            var value = e.layer.feature.z || 0
            map.info.update(e.layer.feature.name+": "+value.toFixed(1)+" ("+vueobj.dataSource.name+" "+vueobj.date+")");
        }).on('mouseout', e=>{
            map.info.update("");
        })
        polygons.addTo(map);

        // add arrows
        let centroid = meanCentroid(vueobj.selectedAreas)
        var notSelectedFeats = vueobj.geojson.features.filter(x=>!vueobj.selectedAreasIds.includes(x.id))
        var topFeatures = notSelectedFeats.sort((a, b)=> b.z - a.z).slice(0, 10)
        var arrows = createArrowsFeatures(topFeatures, centroid, vueobj.dataSource.arrows_incoming)

        function styleArrows(feature) {
            return {
                fillColor: vueobj.dataSource.arrows_color,
                weight: 1,
                opacity: 1,
                color: vueobj.dataSource.arrows_color,
                // dashArray: '3',
                fillOpacity: 0.75
            };
        }

        var arrLayer = L.geoJson(arrows, {style: styleArrows})
        arrLayer.addTo(map);

        // add border to selected areas
        addBorderToSelectedAreas(vueobj, borders=>{
            borders.on('mouseover', e=>{
                vueobj.map.info.update(e.layer.feature.name);
            }).on('mouseout', e=>{
                vueobj.map.info.update("");
            })
        })
    },
    onSelectAreas: function(vueobj) {
        this.paintMap(vueobj)
    },
}



Vue.component('mainmap', {
    name: 'mainmap',
    data: function () {
        return {
            geojson: null,
            mapid: null,
            map: null,
            controller: mapControllerColor,
            selectedAreasIds: null
        }
    },
    props: {
        layer: null,
        dataSource: null,
        date: null,
    },
    computed: {
        selectedAreas: function() {
            if (this.selectedAreasIds) {
                return this.geojson.features.filter(x=>this.selectedAreasIds.includes(x.id))
            } else {
                return null
            }
        }
    },
    methods: {
        updateController: function() {
            if (!this.dataSource) {
                this.controller = mapControllerPlain
            } else if (this.dataSource.type == 'arrows') {
                this.controller = mapControllerArrows
            } else {
                this.controller = mapControllerColor
            }
        },
        onSearchResult: function(result) {
            if (!result) {
                return
            }
            this.selectedAreasIds = [result.id]
            // centerMapOnArea(this.mapid, result)
        }
    },
    watch: {
        layer: {
            immediate: true,
            handler: async function() {
                this.selectedAreasIds = null
                this.geojson = await getLayerGeojson(this.layer)
            }
        },
        dataSource: function() {
            this.updateController()
            this.controller.paintMap(this)
        },
        date: function() {
            this.controller.paintMap(this)
        },
        geojson: function() {
            this.controller.paintMap(this)
        },
        selectedAreasIds: function(val) {
            this.controller.onSelectAreas(this)
        }
    },
    mounted(){
        this.mapid = "mainMap_"+this._uid
    },
    template: `
        <div class="cont">
            <searchbar :geojson="geojson" @selected-result="onSearchResult($event)"></searchbar>
            <div class="chart" :id="mapid" style="height: 80vh;"></div>

            <div v-if="selectedAreasIds && selectedAreasIds.length>0">
                <infobox :date="date" :selectedAreasIds="selectedAreasIds" :selectedAreas="selectedAreas" :dataSource="dataSource"></infobox>
            </div>
            
        </div>
    `
})

