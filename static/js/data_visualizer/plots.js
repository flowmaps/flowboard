
// =====================================  LINE PLOTS  =====================================

function buildLinePlot(data, yfield, xfield='date', currx=null) {
    data = data.sort((a, b)=>a[xfield] < b[xfield] ? 1: -1)
    var trace1 = {
        x: data.map(x=>x[xfield]),
        y: data.map(x=>x[yfield]),
        type: 'scatter',
        hovertemplate: "%{y}<extra></extra>",
    }
    var plotdata = [trace1];
    if (currx && data.find(x=>x[xfield] == currx)) {
        var curry = data.find(x=>x[xfield] == currx)[yfield]
        var trace2 = {
          x: [currx],
          y: [curry],
          mode: 'markers',
          hovertemplate: "%{y}<extra></extra>",
        }
        plotdata.push(trace2)
    }
    var layout = {
        height: 200,
        width: 450,
        margin: {"r":10,"t":25,"l":30,"b":30},
        showlegend: false,
    }
    return {data: plotdata, layout}
}



