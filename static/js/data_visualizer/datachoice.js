
Vue.component('datachoice-radio', {
    name: 'datachoice-radio',
    data: function () {
        return {
            layers: layers,
            dataSources: dataSources,
            date: new Date('2021-01-01'),
            selectedLayer: layers[1],
            selectedDataSource: null,
            layerinputname: null,
            datainputname: null,
        }
    },
    mounted: function () {
        this.layerinputname = "layerinput_"+this._uid
        this.datainputname = "datainput_"+this._uid
    },
    computed: {
        dataSourcesForSelectedLayer: function() {
            return dataSources.filter(d=>d.layer == this.selectedLayer.id)
        },
        dateStr: function () {
            return this.date.toISOString().split('T')[0]
        },
    },
    methods: {

    },
    watch: {
        selectedLayer: function() {
            this.selectedDataSource = null
        }
    },
    template: `
    <div style="overflow-x: hidden !important;">
        <div class="row">
            <div class="col-4">
                <div><v-date-picker v-model='date'/></div>
            </div>
            <div class="col-4">
                Layer:
                <div v-for="layer in layers">
                    <input type="radio" v-model="selectedLayer" :name="layerinputname" :value="layer"> {{layer.name}}
                </div>
            </div>
            <div class="col-4">
                Data:
                <div v-for="dataSource in dataSourcesForSelectedLayer">
                    <input type="radio" v-model="selectedDataSource" :name="datainputname" :value="dataSource"> {{ dataSource.name }}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <mainmap :layer="selectedLayer" :dataSource="selectedDataSource" :date="dateStr"></mainmap>
            </div>
        </div>
    </div>

    `
})


Vue.component('datachoice', {
    name: 'datachoice',
    data: function () {
        return {
            layers: layers,
            dataSources: dataSources,
            date: new Date('2021-01-01'),
            selectedLayer: layers[1],
            selectedDataSource: dataSources.find(x=>x.id == 'ES.covid_cpro.7_100k'),
            layerinputname: null,
            datainputname: null,
            locale: 'es-ES',
            loadingLayers: false,
        }
    },
    mounted: function () {
        this.layerinputname = "layerinput_"+this._uid
        this.datainputname = "datainput_"+this._uid
        this.preloadLayers()
    },
    computed: {
        dataSourcesForSelectedLayer: function() {
            return dataSources.filter(d=>d.layer == this.selectedLayer.id)
        },
        dateStr: function () {
            return this.date.toISOString().split('T')[0]
        },
    },
    methods: {
        preloadLayers: function() {
            this.loadingLayers = true;
            var thiscpy = this
            populateCache(1500, ()=>{thiscpy.loadingLayers = false})
        },
        addOneDay: function() {
            var day = 60 * 60 * 24 * 1000;
            this.date = new Date(this.date.getTime() + day);
        },
        substractOneDay: function() {
            var day = 60 * 60 * 24 * 1000;
            this.date = new Date(this.date.getTime() - day);
        },
        addOneMonth: function() {
            this.date = new Date(this.date.setMonth(this.date.getMonth()+1));
        },
        substractOneMonth: function() {
            this.date = new Date(this.date.setMonth(this.date.getMonth()-1));
        }
    },
    watch: {
        selectedLayer: function() {
            if (this.selectedDataSource != undefined) {
                this.selectedDataSource = this.dataSourcesForSelectedLayer.find(d=>d.name == this.selectedDataSource.name) || null
            }
        }
    },
    template: `
    <div style="overflow-x: hidden !important;">
        <div id="data_visualizer_selectors" class="row" style="padding-bottom: 10px;">
            <div class="col-4">
                <nobr>
                    <span data-i18n='data_visualizer.date'></span>:
                    <span class="btn-group">
                        <button v-on:click="substractOneMonth()" class="button"><<</button>
                        <button v-on:click="substractOneDay()" class="button"><</button>
                    </span><span class="datebox">
                        <v-date-picker v-model="date" :locale="locale">
                          <template v-slot="{ inputValue, inputEvents }">
                            <input
                              class="bg-white border px-2 py-1 rounded"
                              :value="inputValue"
                              v-on="inputEvents"
                            />
                          </template>
                        </v-date-picker>
                    </span><span class="btn-group">
                        <button v-on:click="addOneDay()" class="button">></button>
                        <button v-on:click="addOneMonth()" class="button">>></button>
                    </span>
                </nobr>
            </div>
            <div class="col-4">
                <nobr>
                <span data-i18n='data_visualizer.layer'></span>:
                <select class="select_layer_data" name="layer" id="layer" v-model="selectedLayer" title="select number">
                    <option v-for="layer in layers" :value="layer"> {{layer.name}} </option>
                </select>
                </nobr>
            </div>
            <div class="col-4">
                <nobr>
                <span data-i18n='data_visualizer.data'></span>:
                <select class="select_layer_data" name="datasource" id="datasource" v-model="selectedDataSource">
                    <option v-for="dataSource in dataSourcesForSelectedLayer" :value="dataSource"> {{dataSource.name}} </option>
                </select>
                </nobr>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <mainmap :layer="selectedLayer" :dataSource="selectedDataSource" :date="dateStr"></mainmap>
            </div>
        </div>
        <div class="loadingmsgparent">
            <div class="loadingmsg">{{ loadingLayers ? "cargando capas..." : "" }}</div>
        </div>
    </div>

    `
})