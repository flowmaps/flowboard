
Vue.component('infobox', {
    name: 'infobox',
    data: function () {
        return {
            plotid: null,
            collapseid: null,
        }
    },
    props: {
        dataSource: null,
        selectedAreasIds: null,
        selectedAreas: null,
        date: null,
    },
    mounted() {
        this.plotid = "plot_"+this._uid
        this.collapseid = "collapse_"+this._uid
    },
    methods: {
        drawLinePlot: async function() {
            if (this.dataSource && this.selectedAreasIds && this.dataSource.drawLinePlot) {
                // build url
                var filters = Object.assign({}, this.dataSource.filters) // copy
                filters[this.dataSource.loc || 'id'] = {"$in": this.selectedAreasIds}
                var pipeline = [
                    {"$match": filters},
                    {
                        "$group": {
                            "_id": "$date",
                            [this.dataSource.field]: {
                                "$sum": "$"+this.dataSource.field
                            }
                        }
                    }, {
                        "$project": {
                            "_id": 0,
                            "date": "$_id",
                            [this.dataSource.field]: 1,
                        }
                    }
                ]
                let url = buildApiUrl("aggregate", {"collection": this.dataSource.collection, "pipeline": pipeline})

                // get data
                let data = await fetchAllPages(url)

                // plot
                var fig = buildLinePlot(data, this.dataSource.field, "date", this.date)
                Plotly.react(this.plotid, fig.data, fig.layout)
            } else {
                if (document.getElementById(this.plotid))
                    Plotly.purge(this.plotid)
            }
        },
        hidesidebar() {
            document.getElementById('rightsidebar').style.display = "none";
            document.getElementById('sidebaropenbutton').style.display = "block";
        },
        showsidebar() {
            document.getElementById('rightsidebar').style.display = "block";
            document.getElementById('sidebaropenbutton').style.display = "none";
        },
    },
    watch: {
        dataSource: function() {
            this.drawLinePlot()
        },
        selectedAreasIds: {
            immediate: true,
            handler: function() {
                this.drawLinePlot()
            }
        },
        date: function() {
            this.drawLinePlot()
        }
    },
    template: `
        <div>
            <div id="rightsidebar" class="rightsidebar">
                <button v-on:click="hidesidebar()" class="sidebarbutton btn btn-light">Ocultar panel</button>
                <div class="rightsidebar_text">
                    <b> {{dataSource ? dataSource.name : ""}} </b>
                    <div>{{selectedAreas ? (selectedAreas.length == 1 ? selectedAreas[0].name : selectedAreas.length + " areas seleccionadas") : ""}}</div>
                    <div>{{selectedAreas.length == 1 ? "id: " + selectedAreas[0].id : "ids: " + JSON.stringify(selectedAreas.map(x=>x.id))}}</div>
                </div>
                <div class="chart" :id="plotid"></div>
                <!--
                <pre>{{selectedAreas ? JSON.stringify(selectedAreas.map(x=>x.properties), null, 2) : ""}}</pre>
                -->
            </div>
            <div id="sidebaropenbutton" class="rightsidebaropenbutton">
                <button v-on:click="showsidebar()" class="sidebarbutton btn btn-light">&#x25C2;  Mostrar panel</button>
            </div>
        </div>
    `
})
