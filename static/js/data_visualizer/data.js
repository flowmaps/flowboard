
async function fetch_incidence_by_100k(v) {
    let field = v.dataSource.field

    // fetch population data
    let filters1 = {
        "type": "population",
        "layer": v.dataSource.filters["layer"],
        "date": v.date,
    }
    let url1 = buildApiUrl('layers.data.consolidated', filters1)

    // fetch covid data
    let filters2 = {
        "type": "covid19",
        "ev": v.dataSource.filters["ev"],
        "layer": v.dataSource.filters["layer"],
        "date": v.date,
    }
    let url2 = buildApiUrl('layers.data.consolidated', filters2)
    
    var data = await Promise.all([fetchAllPages(url1), fetchAllPages(url2)])

    let popdata = data[0]
    let pop = {};
    popdata.forEach(d=>{
        pop[d["id"]] = d["population"]
    })

    let coviddata = data[1]
    let values = {};
    coviddata.forEach(d=>{
        values[d["id"]] = pop[d["id"]] ? 100000 * d[field] / pop[d["id"]] : 0
    })

    return values
}


async function fetch_mobility(v) {
    let date = v.date
    let dataSource = v.dataSource
    let selectedAreasIds = v.selectedAreasIds
    let loc = dataSource.loc
    let field = dataSource.field
    var filters = Object.assign({}, dataSource.filters) // copy
    if (dataSource.daily) {
        filters['date'] = date
    }

    filters[dataSource.arrows_center_field] = {"$in": selectedAreasIds}
    var pipeline = [
        {"$match": filters},
        {
            "$group": {
                "_id": "$"+loc,
                [field]: {
                    "$sum": "$"+field
                }
            }
        }, {
            "$project": {
                "_id": 0,
                [loc]: "$_id",
                [field]: 1,
            }
        }
    ]
    let url = buildApiUrl("aggregate", {"collection": dataSource.collection, "pipeline": pipeline})
    let data = await fetchAllPages(url)
    let values = {};
    data.forEach(d=>{
        values[d[loc]] = selectedAreasIds.includes(d[loc]) ? 0 : d[field]
    })
    return values
}


async function fetch_outgoing_risk(v) {
    let loc = v.dataSource.loc
    let field = v.dataSource.field
    let filters = {
        "sources": v.selectedAreasIds,
        "date": v.date,
        "source_layer": v.dataSource.filters["source_layer"],
        "target_layer": v.dataSource.filters["target_layer"],
        "ev": v.dataSource.filters["ev"],
    }
    let url = buildApiUrl("outgoing_risk_multiple_sources", filters)
    let data = await fetchAllPages(url)
    let values = {};
    data.forEach(d=>{
        values[d[loc]] = v.selectedAreasIds.includes(d[loc]) ? 0 : d[field]
    })
    return values
}


async function fetch_incoming_risk(v) {
    let loc = v.dataSource.loc
    let field = v.dataSource.field
    let filters = {
        "targets": v.selectedAreasIds,
        "date": v.date,
        "source_layer": v.dataSource.filters["source_layer"],
        "target_layer": v.dataSource.filters["target_layer"],
        "ev": v.dataSource.filters["ev"],
    }
    let url = buildApiUrl("incoming_risk_multiple_targets", filters)
    let data = await fetchAllPages(url)
    let values = {};
    data.forEach(d=>{
        values[d[loc]] = v.selectedAreasIds.includes(d[loc]) ? 0 : d[field]
    })
    return values
}
