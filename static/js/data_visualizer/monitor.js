
Vue.component('monitor', {
    name: 'monitor',
    data: function () {
        return {
            data: [],
        }
    },
    mounted: function () {
        let url = apiURL + '/monitor'
        fetch(url).then(result=>result.json()).then(response=>{
            if (response && response._items) {
                this.data = response._items
            }
        })
    },
    template: `
        <div>
            <h3>Monitor data sources</h3>
            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Last available date</th>
                        <th>Collection</th>
                        <th>Last data added at</th>
                        <!--
                            <th>Raw info</th>
                        -->
                    </tr>
                </thead>
                    <tr v-for="doc in data">
                        <td> {{doc.spec.name}} </td>
                        <td v-bind:class="[doc.delay > 7 ? 'highlightred' : (doc.delay > 3 ? 'highlightorange' : 'highlightgreen')]""> {{doc.last_avail_date}}<br>({{doc.delay}} days delay) </td>
                        <td> {{doc.spec.collection}} </td>
                        <!--
                            <td> <pre style="overflow: scroll; height: 100px; width: 300px;">{{JSON.stringify(doc, null, 2)}}</pre> </td>
                        -->
                        <td> {{doc.raw.updated_at}} </td>
                    </tr>
                <tbody>

                </tbody>
            </table>

        </div>

    `
})
