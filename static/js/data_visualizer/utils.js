var layerCache = {}


async function getLayerGeojson(layer) {
    if (!layerCache[layer.id]) {
        let response = await fetch(layer.url)
        layerCache[layer.id] = await response.json()
    } else {
        console.log('cache hit!', layer.id)
    }
    return layerCache[layer.id]
}


function populateCache(wait_ms=2000, onDone) {
    setTimeout(function() { 
        Promise.all(layers.map(layer => {
            console.log("Prefetching", layer.id)
            return getLayerGeojson(layer)
        })).then(responses => {
            console.log("Finished prefetch all layers")
            onDone()
        })
    }, wait_ms)
}


async function fetchAllPages(url) {
    const data = []
    do {
        let response = await (await fetch(url)).json()
        data.push(...response._items)
        url = (response._links && response._links.next) ? apiURL+"/"+response._links.next.href : null
    } while (url)
    return data
}


function buildApiUrl(collection, filters, max_results=500, projection={}) {
    return apiURL+'/'+collection+'?where='+JSON.stringify(filters)+'&max_results='+max_results+'&projection='+JSON.stringify(projection)
}


function synchronizeMaps(id1, id2) {
    _syncWithOtherMap(id1, id2)
    _syncWithOtherMap(id2, id1)
}


function _syncWithOtherMap(id1, id2) {
    var syncInProgress = false;
    g1 = document.getElementById(id1)
    g2 = document.getElementById(id2)

    g1.on('plotly_relayout', (e) => {
        // update zoom in the other map
        var update = {
            'mapbox.zoom': e['mapbox.zoom'],
            'mapbox.center': e['mapbox.center']
        }
        Plotly.update(id2, {}, update)
    });

    g1.on('plotly_relayouting', (e) => {
        // do not update the zoom too frequently
        if (g2.layout.mapbox.zoom != e['mapbox.zoom']) {
            if (syncInProgress | Math.abs(g2.layout.mapbox.zoom - e['mapbox.zoom']) < 0.2) {
                return
            }
        }
        syncInProgress = true
        var update = {
            'mapbox.zoom': e['mapbox.zoom'],
            'mapbox.center': e['mapbox.center']
        }
        Plotly.update(id2, {}, update).then(gd=>{syncInProgress = false})
    });
}


function groupBySum(docs, loc, key) {
    var accum = {}
    docs.forEach(d=>{
        accum[d[loc]] = (accum[d[loc]] || 0) + d[key]
    })
    return Object.entries(accum).map(x=>{
        var d = {}
        d[loc] = x[0]
        d[key] = x[1]
        return d
    })
}

// populateCache(wait_ms=1500)

