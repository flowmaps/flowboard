
// =====================================  ARROWS  =====================================

function round(num, decimals=0){
    var k = Math.pow(10, decimals)
    return Math.round((num + Number.EPSILON) * k) / k
}

function rotate(a, theta) {
    return [a[0]*Math.cos(theta) - a[1]*Math.sin(theta), a[0]*Math.sin(theta) + a[1]*Math.cos(theta)];
}

function module(a, b) {
    var v = [b[0]-a[0], b[1]-a[1]]                            // get direction vector
    return Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
}

function createArrow(a, b, dist=0.2, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newb = [b[0]-u[0]*dist, b[1]-u[1]*dist]       // place the arrow end point a bit before the real end 
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [a, newb, [newb[0]+s1[0], newb[1]+s1[1]], newb, [newb[0]+s2[0], newb[1]+s2[1]]]
}


function createOutgoingArrow(a, b, dist=0.1, markerSize=0.06) {
    // dist: how far place the arrow end from the destination
    // markerSize: arrow marker size
    var angle = Math.atan2(a[1]-b[1], a[0]-b[0]);
    var v = [b[0]-a[0], b[1]-a[1]]                    // get direction vector
    var m = Math.sqrt(Math.pow(v[0], 2) + Math.pow(v[1], 2))  // module
    var u = [v[0]/m, v[1]/m]                          // get unitary vector in the direction
    var newa = [a[0]+u[0]*dist, a[1]+u[1]*dist]       // place the arrow start point a bit before the real start 
    var s1 = rotate([markerSize, markerSize], angle)  // "sides" of the arrow. Applied to a base vector in left direction <--, and rotated to match the correct angle
    var s2 = rotate([markerSize, -markerSize], angle)
    return [newa, b, [b[0]+s1[0], b[1]+s1[1]], b, [b[0]+s2[0], b[1]+s2[1]]]
}


function createArrowsFeatures(features, center, incoming) {
    var lines = features.map(x => incoming ? [x.centroid, center] : [center, x.centroid]);
    var lengths = lines.map(x => module(x[0], x[1]))  // calculate the size and distance of arrows dinamically
    var markerSize = Math.min(...lengths)/10
    var dist = Math.min(...lengths)/4
    if (incoming) {
        var arrows = lines.map(x => createArrow(x[0], x[1], dist=dist, markerSize=markerSize));
    } else {
        var arrows = lines.map(x => createOutgoingArrow(x[0], x[1], dist=dist, markerSize=markerSize));
    }
    return {
        "type": "MultiLineString", 
        "coordinates": arrows
    }
}


function meanCentroid(features) {
    const average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length
    return [average(features.map(x=>x.centroid[0])), average(features.map(x=>x.centroid[1]))]
}
