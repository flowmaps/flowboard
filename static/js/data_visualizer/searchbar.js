
Vue.component('searchbar', {
    name: 'searchbar',
    data: function () {
        return {
            search: "",
            results: [],
            selectedResult: null,
            isOpen: false,
            arrowCounter: 0,
        }
    },
    props: {
        geojson: {features: [{properties: {rotulo: 'default'}}]},
    },
    methods: {
        onChange: function() {
            if (!this.search) {
                this.isOpen = false
                this.results = []
                return
            }
            this.selectedResult = null
            this.results = this.geojson.features.filter((feat) => feat.name && feat.name.toLowerCase().indexOf(this.search.toLowerCase()) > -1).slice(0, 10)
            if (this.results.length == 0) {
                // try search by id
                this.results = this.geojson.features.filter((feat) => feat.id.indexOf(this.search) > -1).slice(0, 10)
            }
            this.isOpen = true
        },
        setResult(result) {
            this.selectedResult = result
            this.isOpen = false
            this.$emit('selected-result', result)
        },
        onEnter() {
            if (this.selectedResult) {
                this.$emit('selected-result', this.selectedResult)
            } else if (this.results) {
                this.selectedResult = this.results[this.arrowCounter]
                this.$emit('selected-result', this.selectedResult)
            }
            this.isOpen = false
            this.arrowCounter = 0
        },
        onArrowDown() {
            if (this.arrowCounter < this.results.length) {
                this.arrowCounter = this.arrowCounter + 1;
            }
        },
        onArrowUp() {
            if (this.arrowCounter > 0) {
                this.arrowCounter = this.arrowCounter -1;
            }
        },
        handleClickOutside(evt) {
            if (!this.$el.contains(evt.target)) {
              this.isOpen = false;
              this.arrowCounter = 0;
            }
        }
    },
    watch: {
        selectedResult: function() {
            if (this.selectedResult && this.selectedResult.name) {
                this.search = this.selectedResult.name
            }
        },
        geojson: function() {
            this.search = ""
            this.selectedResult = null
            this.results = []
        }
    },
    mounted() {
        document.addEventListener('click', this.handleClickOutside)
    },
    destroyed() {
        document.removeEventListener('click', this.handleClickOutside)
    },
    template: `
        <div class="autocomplete">
            <input 
              type="text"
              @input="onChange"
              v-model="search"
              @keydown.down="onArrowDown"
              @keydown.up="onArrowUp"
              @keydown.enter="onEnter"
              placeholder="Search"></input>
            <ul id="autocomplete-results" v-show="isOpen" class="autocomplete-results">
                <li 
                  v-for="(result, i) in results"
                  :key="i"
                  @click="setResult(result)"
                  class="autocomplete-result"
                  :class="{ 'is-active': i === arrowCounter }"
                >
                    {{(result.name == result.id) ? result.name : result.name + " (id=" + result.id + ")"}}
                </li>
            </ul>
        </div>
    `

})

