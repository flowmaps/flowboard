// =====================================  AESTHETIC CHANGES  =====================================
let colorIncidence = '#000059' // #050559 #03032e #030333
let colorBase = '#f4f4f4'

// TABS
var has_to_recreate_global_map = false;
var has_to_recreate_local_map = true;
function draw_plotly_on_show_tab() {
    // Initialize the Plotly graphs when the tab is activated in order to avoid an issue that happens
    // when Plotly doesn't know exactly the size of the available div (it's hidden)

    $('#abs-tab').on('shown.bs.tab', function (e) {
        console.log('ACTIVATING ABS TAB');
        if (has_to_recreate_local_map) {
            createLocalMap(selected_province);
            create_local_plot(selected_province);
            has_to_recreate_local_map = false;
        }
    });

    $('#spain-tab').on('shown.bs.tab', function (e) {
        console.log('ACTIVATING SPAIN TAB');
        if (has_to_recreate_global_map) {
            createGlobalMap();
            has_to_recreate_global_map = false;
        }
    });
}
draw_plotly_on_show_tab();


// =====================================  GLOBAL VARIABLES  =====================================
var selected_province = "08";
var ply_config = { displayModeBar: false }


// =====================================  MAP MANAGEMENT  =====================================
function round(num, decimals=0){
    var k = Math.pow(10, decimals)
    return Math.round((num + Number.EPSILON) * k) / k
}

function splitTextInLines(text, wordsPerLine=5, lineSep='\n') {
    return [...Array(parseInt(text.split(' ').length/wordsPerLine + 1)).keys()].map(x=>text.split(' ').slice(x*wordsPerLine,(x+1)*wordsPerLine)).map(x=>x.join(' ')).join(lineSep)
}

function buildCustomData(id, covidData, provincesNames, popData) {
    return [
        provincesNames[id],
        round(popData[id].population),
        covidData[id].new_cases,
        round(100000*covidData[id].new_cases/popData[id].population, 2),
        covidData[id].active_cases_7,
        covidData[id].active_cases_14,
        round(100000*covidData[id].active_cases_14/popData[id].population, 2)
    ]
}

function create_data_for_map(locations, covidData, popData, provincesNames, provinciasGeojson) {
    return {
			type: "choroplethmapbox",
			locations: locations,
            z: locations.map(x=>round(100000*covidData[x].active_cases_14/popData[x].population, 2)),
            zmin: 0,
            zmax: 600,
			customdata: locations.map(x=>buildCustomData(x, covidData, provincesNames, popData)),
			geojson: provinciasGeojson,
            // hovertemplate: "Province: %{customdata[0]}<br>Population: %{customdata[1]}<br>New Cases:<br>- Incidence: %{customdata[2]}<br>- Incidence per 100k population: %{customdata[3]}<br>Accumulated Cases:<br>- Incidence (7 days): %{customdata[4]}<br>- Incidence (14 days): %{customdata[5]}<br>- Incidence (14 days) per 100k population: %{customdata[6]}<extra></extra>",
            hovertemplate: "%{customdata[0]}<extra></extra>",
            // hoverinfo: 'none',
			marker: {
				opacity: 0.6,
			},
			colorbar: {
				title: {
					text: "Incidence (14 days) per 100k population",
					side: "right"
				},
			},
			colorscale: [['0.0', colorBase], ['1.0', colorIncidence]], // "Reds"
    };
}

function createGlobalMap() {
    let filters = {
        "ev": "ES.covid_cpro",
        "start_date": date_str,
        "end_date": date_str,
    }
    let filters2 = {
        "type": "population",
        "date": '2020-03-01',
        "layer": "cnig_provincias",
    }

	Promise.all([
		fetch(staticRoute+'data/cnig_provincias.geojson'),
        fetch(apiURL+'/incidence/?where='+JSON.stringify(filters)),
		fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters2)+'&max_results=500'),
	]).then(function (responses) {
		return Promise.all(responses.map(function (response) {
			return response.json();
		}));
	}).then(function (data) {
		var provinciasGeojson = data[0];

		// covidData: {1: {"active_cases_14": 3, ...}, 2: {...}, 3: {...}}
        var covidData = {};
        data[1]._items[0].data.forEach(d => {
            covidData[d.id] = d;
        });

        // popData: {1: 300, 2: 450, 3: 403, ...}
        var popData = {};
        data[2]._items.forEach(d => {
            popData[d.id] = d;
        })

        var provincesNames = {};
        provinciasGeojson.features.forEach(feat => provincesNames[feat.id] = feat.name);
        var locations = Object.keys(covidData);

		var layout = {
			mapbox: {
				center: {
					lon: -4, lat: 40.095178477814555
				},
				zoom: 5,
				style: 'carto-positron',
				layers: [{
                    "color": "black",
                    "line": {"width": 2.5},
                    "opacity": 1,
                    "sourcetype": "geojson",
                    "type": "line",
                    "source": {
                        "type": "FeatureCollection",
                        "features": [provinciasGeojson.features.find(y=>y.id == selected_province)]
                    },
                }],
			},
			height: 550,
			margin: {"r":0,"t":0,"l":0,"b":0},
		};
        var data = [create_data_for_map(locations, covidData, popData, provincesNames, provinciasGeojson)];

	    Plotly.newPlot('mapgraph', data, layout, ply_config).then(gd => {
			gd.on('plotly_click', (e) => {
				selected_province = e.points[0].location;

				// Border of the province selected bigger
                $('#mapgraph')[0].layout.mapbox.layers = [
                    {
                        "color": "black",
                        "line": {"width": 2.5},
                        "opacity": 1,
                        "sourcetype": "geojson",
                        "type": "line",
                        "source": {
                            "type": "FeatureCollection",
                            "features": [provinciasGeojson.features.find(y=>y.id == selected_province)]
                        }
                    }
                ];
                Plotly.redraw('mapgraph')

				// Open abs tab
				has_to_recreate_local_map = true;
				$("#abs-tab").click();
			});

            gd.on('plotly_hover', (e) => {
                var customdata = e.points[0].customdata
                var z = e.points[0].z
                var text = `Province: ${customdata[0]}<br>` +
                    `Population: ${customdata[1]}<br>` +
                    `Daily incidence:<br>` +
                    ` - Daily incidence: ${customdata[2]}<br>` +
                    ` - Daily incidence per 100k population: ${customdata[3]}<br>` +
                    `Accumulated incidence:<br>` +
                    ` - Incidence (7 days): ${customdata[4]}<br>` +
                    ` - Incidence (14 days): ${customdata[5]}<br>` +
                    ` - Incidence (14 days) per 100k population: ${customdata[6]}<br>`

                document.getElementById('hoverinfo').innerHTML = text;
                document.getElementById('hoverinfo').style.visibility = 'visible';
            })

            gd.on('plotly_unhover', (e) => {
                document.getElementById('hoverinfo').style.visibility = 'hidden';
            });
		})

	}).catch(function (error) {
		// Hide div in case of fail
        $('#incidence_map_div').hide()
        console.log('Incidence map not available.')
	});
}

function createLocalMap(province_id) {
    var ccaa_id = province_to_ccaa[province_id];
    if (! (ccaa_id in ccaa_data_sublayer))  {
        alert('Sorry, we do not have high resolution Covid-19 data for this province.');
        $('#localmapdiv').hide();
        return;
    } else {
        $('#localmapdiv').show();
        var local_layer_id = ccaa_data_sublayer[ccaa_id];
        var ev = layer_to_ev[local_layer_id];
    }

    // Create local map
    var filters = {
        "type": "covid19",
        "date": date_str,
        "ev": ev,
    }
    let filters2 = {
        "type": "population",
        "date": '2020-03-01',
        "layer": local_layer_id,
    }

    Promise.all([
        fetch(staticRoute+'data/hires_province_' + province_id + '.geojson'),
        fetch(staticRoute+'data/cnig_provincias.geojson'),
        fetch(apiURL+'/layers.data.consolidated/?where=' + JSON.stringify(filters) + "&max_results=500"),
        fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters2)+'&max_results=500'),
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        var province_lat, province_lon;
        [province_lon, province_lat] = province_centroids[province_id];
        var geojson = data[0];
        var provincesGeojson = data[1];

        var covidData = data[2]['_items'];
        if (covidData.length == 0) {
            alert('Sorry, we do not have high resolution Covid-19 data for this date. Change it in the left calendar.');
            return;
        }
        var covidDataIndex = {}; covidData.forEach(d => {covidDataIndex[d.id] = d})

        var popData = {};
        data[3]._items.forEach(d => {
            popData[d.id] = d;
        })

        var data = [
            {
                type: "choroplethmapbox",
                locations: geojson.features.map(x=>x.id),
                z: geojson.features.map(x=>
                    round(100000*((covidDataIndex[x.id] || {}).active_cases_14 || 0) / popData[x.id].population, 2)
                ),
                customdata: geojson.features.map(x=>[
                    x.name,
                    round(popData[x.id].population),
                    (covidDataIndex[x.id] || {}).new_cases || 0,
                    round(100000 * ((covidDataIndex[x.id] || {}).new_cases || 0) / popData[x.id].population, 2),
                    (covidDataIndex[x.id] || {}).active_cases_7 || 0,
                    (covidDataIndex[x.id] || {}).active_cases_14 || 0,
                    round(100000*((covidDataIndex[x.id] || {}).active_cases_14 || 0) / popData[x.id].population, 2)
                ]),
                geojson: geojson,
                hovertemplate: "%{customdata[1]}<extra></extra>",
                marker: {
                    opacity: 0.6,
                },
                colorbar: {
                    title: {
                        text: "Incidence (14 days) per 100k population",
                    },
                    x: 0,
			    },
                colorscale: [['0.0', colorBase], ['1.0', colorIncidence]],
                hoverlabel: {align: "left"},
                zmin: 0,
                zmax: 1000,
            },
        ];

        var layout = {
            mapbox: {
                center: {
                    lon: province_lon, lat: province_lat
                },
                zoom: 7.5,
                style: 'carto-positron',
            },
            height: 500,
            margin: {"r":0,"t":0,"l":0,"b":0},
        };

        Plotly.newPlot('localMap', data, layout, ply_config).then(gd => {
            gd.on('plotly_hover', (e) => {
                var customdata = e.points[0].customdata
//                var text = "Zone: " + customdata[1] + "<br>" +
//                           "Population: " + customdata[2] + "</br>" +
//                           "Incidence (14 days): " + customdata[3] + "<br>" +
//                           "Incidence (14 days) per 100k population: " + customdata[4]

                var text = `Zone: ${customdata[0]}<br>` +
                    `Population: ${customdata[1]}<br>` +
                    `Daily incidence:<br>` +
                    ` - Daily incidence: ${customdata[2]}<br>` +
                    ` - Daily incidence per 100k population: ${customdata[3]}<br>` +
                    `Accumulated incidence:<br>` +
                    ` - Incidence (7 days): ${customdata[4]}<br>` +
                    ` - Incidence (14 days): ${customdata[5]}<br>` +
                    ` - Incidence (14 days) per 100k population: ${customdata[6]}<br>`

                $('#hoverinfo_localmap')[0].innerHTML = text;
                $('#hoverinfo_localmap')[0].style.visibility = 'visible';
            }),

            gd.on('plotly_unhover', (e) => {
                $('#hoverinfo_localmap')[0].style.visibility = 'hidden';
            });
        });

        // Create Datatable if it doesn't exist
        if ( ! $( '#local_incidence_table' ).hasClass( "dataTable" ) ) {
            create_local_incidence_table();
        }

        // Update the datatable with the data
        update_local_incidence_table(
            geojson.features.map(x=> [
                x.name,
                round(100000*((covidDataIndex[x.id] || {}).active_cases_14 || 0) / popData[x.id].population, 2),
                (covidDataIndex[x.id] || {}).active_cases_14 || 0,
                (covidDataIndex[x.id] || {}).active_cases_7 || 0,
            ])
        );

    }).catch(function (error) {
		// Hide div in case of fail
        $('#localmapdiv').hide()
        console.log('Local Incidence map not available.')
	});
}



// ================================ ANIMATION MANAGEMENT ==========================================
function create_data_for_animation(date, locations, covidDataWithDates, popData, provincesNames, provinciasGeojson) {
    return {
			type: "choroplethmapbox",
			locations: locations,
            z: locations.map(x=>round(100000*covidDataWithDates[date][x].active_cases_14/popData[x].population, 2)),
            zmin: 0,
            zmax: 600,
			customdata: locations.map(x=>buildCustomData(x, covidDataWithDates[date], provincesNames, popData)),
			geojson: provinciasGeojson,
            // hovertemplate: "Province: %{customdata[0]}<br>Population: %{customdata[1]}<br>New Cases:<br>- Incidence: %{customdata[2]}<br>- Incidence per 100k population: %{customdata[3]}<br>Accumulated Cases:<br>- Incidence (7 days): %{customdata[4]}<br>- Incidence (14 days): %{customdata[5]}<br>- Incidence (14 days) per 100k population: %{customdata[6]}<extra></extra>",
            hovertemplate: "%{customdata[0]}<extra></extra>",
            // hoverinfo: 'none',
			marker: {
				opacity: 0.6,
			},
			colorbar: {
				title: {
					text: "Incidence (14 days) per 100k population",
					side: "right"
				},
				thickness: 20,
				x: 0,
			},
			colorscale: [['0.0', colorBase], ['1.0', colorIncidence]], // "Reds"
    };
}

function update_data_for_animation(date, locations, covidDataWithDates, popData, provincesNames) {
    return {
        type: "choroplethmapbox",
        customdata: locations.map(x=>buildCustomData(x, covidDataWithDates[date], provincesNames, popData)),
        hovertemplate: "%{customdata[0]}<extra></extra>",
        z: locations.map(x=>round(100000*covidDataWithDates[date][x].active_cases_14/popData[x].population, 2)),
    };
}

function create_animation_items(layout, min_date, max_date, locations, covidDataWithDates, popData, provincesNames) {
    // Create a structure that can be inserted in a plotly layout in order to attach a temporal evolution of cases
    // in Spain between [`min_date`,`max_date`], taking only the 1st of every month for clarity

    // Create update menus buttons
    var updatemenus = [{
      type: 'buttons',
      showactive: false,
      x: 0.95,
      y: 0.12,
      xanchor: 'right',
      yanchor: 'top',
      direction: 'left',
      pad: {t: 1, r: 1},
      buttons: [
      {
          label: 'Play',
          method: 'animate',
          args: [null, {
              fromcurrent: true,
              frame: {redraw: true, duration: 500},
          }]
      }, {
          label: 'Pause',
          method: 'animate',
          args: [[null], {
              mode: 'immediate',
              frame: {redraw: true, duration: 0}
          }]
      }]
    }]
    layout['updatemenus'] = updatemenus;

    // Create sliders and frames
    var steps = [];
    var frames = [];

    var min_date_object = new Date(min_date);
    var max_date_object = new Date(max_date);
    var last_date_represented = null;
    var date = min_date_object;
    while (date < max_date_object) {
        const ye = new Intl.DateTimeFormat('en', { year: '2-digit' }).format(date);
        const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(date);
        const day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);

        var dd = String( date.getDate() ).padStart(2, '0');
        var mm = String( date.getMonth() + 1 ).padStart(2, '0'); //January is 0!
        var yyyy = String( date.getFullYear() );
        const date_formatted = yyyy + '-' + mm + '-' + dd
        const label = day + '/' + mo + '/' + ye ;

        // Create slider steps
        steps.push({
            label: label,
            method: 'animate',
            args: [[label], {
                mode: 'immediate',
                frame: {redraw: true, duration: 500},
                transition: {duration: 500},
            }],
        });
        // 3531

        // Create frames of each step
        frames.push({
            name: label,
            data: [update_data_for_animation(date_formatted, locations, covidDataWithDates, popData, provincesNames)],
        });

        // Storing the last day represented
        last_date_represented = date_formatted;

        // Increase the date 1 month
        date.setDate(date.getDate() + 7);
    }

    var sliders = {
        pad: {t: 1, b:10, l:40},
        y: 0.1,
        active: frames.length - 1,
        currentvalue: {
            visible: true,
            prefix: 'Date: ',
            xanchor: 'left',
            font: {size: 20, color: '#666'},
          },
        steps: steps,
    };
    layout['sliders'] = [sliders];
    return [frames, last_date_represented];
}

function createAnimation() {
    if ( $( '#animation_incidence' ).hasClass( "js-plotly-plot" ) ) {
        return;
    }

    let filters = {
        "ev": "ES.covid_cpro",
    }
    let filters2 = {
        "type": "population",
        "date": '2020-03-01',
        "layer": "cnig_provincias",
    }

	Promise.all([
		fetch(staticRoute+'data/cnig_provincias.geojson'),
        fetch(apiURL+'/incidence/?where='+JSON.stringify(filters)),
		fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters2)+'&max_results=500'),
	]).then(function (responses) {
		return Promise.all(responses.map(function (response) {
			return response.json();
		}));
	}).then(function (data) {
		var provinciasGeojson = data[0];

		// covidDataWithDates: {"2020/03/01": {1: {"active_cases_14": 3, ...}, 2: {...}, 3: {...}}, "2020/03/02": ...}
        var covidDataWithDates = {};
        data[1]._items.forEach(d => {
            var covid_object = {};
            d.data.forEach(item => {
                covid_object[item.id] = item
            })
            covidDataWithDates[d._id] = covid_object;
        });

        // popData: {1: 300, 2: 450, 3: 403, ...}
        var popData = {};
        data[2]._items.forEach(d => {
            popData[d.id] = d;
        })

        var provincesNames = {};
        provinciasGeojson.features.forEach(feat => provincesNames[feat.id] = feat.name);
        var locations = Object.keys(covidDataWithDates[min_date]);

		var layout = {
			mapbox: {
				center: {
					lon: -4, lat: 40.095178477814555
				},
				zoom: 5,
				style: 'carto-positron',
			},
			// width: 800,
			height: 550,
			margin: {"r":0,"t":0,"l":0,"b":0},
		};
        var frames, last_date_represented;
        [frames, last_date_represented] = create_animation_items(layout, min_date, max_date, locations, covidDataWithDates, popData, provincesNames);
        var data = [create_data_for_animation(last_date_represented, locations, covidDataWithDates, popData, provincesNames, provinciasGeojson)];

	    Plotly.plot('animation_incidence', data, layout, ply_config);
	    Plotly.addFrames('animation_incidence', frames)

	}).catch(function (error) {
		// Hide div in case of fail
        $('#incidence_map_div').hide()
        console.log('Incidence map not available.')
	});
}


// =====================================  DATATABLE MANAGEMENT  =====================================
function create_local_incidence_table() {
    // Create the datatable object for future data insertion
    column_names = ['zone', 'incidence_14_days_100k_population', 'incidence_14_days', 'incidence_7_days']
    columns_rename = {
        'zone': i18next.t('datatable.zone'),
        'incidence_14_days_100k_population': i18next.t('datatable.incidence_14_days_100k_population'),
        'incidence_14_days': i18next.t('datatable.incidence_14_days'),
        'incidence_7_days': i18next.t('datatable.incidence_7_days'),
    }
    var table = $('#local_incidence_table').DataTable({
        data: [],
        columns: column_names.map(function(column) { return {title: columns_rename[column]} }),
        order: [[1, 'desc']],
        columnDefs: [
            {"className": "dt-center", "targets": "_all"},
            { "width": "25%", "targets": [0,1,2,3] },
        ],
        pageLength : 10,
        // pagingType: "numbers",
        bLengthChange: false,
        bFilter: false,
        bInfo: false,
    });
}

function update_local_incidence_table(data) {
    var datatable = $('#local_incidence_table').DataTable();
    datatable.clear();
    datatable.rows.add(data);
    datatable.columns.adjust();
    datatable.draw();
}


// =====================================  PLOT MANAGEMENT  =====================================
function create_spain_plot() {
    // SPAIN PLOT DAILY CASES vs TIME
    let risk_filters = {type: "covid19", ev: "ES.covid_cpro.agg_ES"};
    let url = apiURL + '/layers.data.consolidated/?where=' + JSON.stringify(risk_filters)
                                                           + '&projection={"detailed_risk": 0}&max_results=500'
                                                           + '&sort=[("date", 1)]';
    let events_filters = {
        'id': 'ES',
        'layer': 'world',
        'ev': 'lockdown_wikipedia',
    }
    Promise.all([
        fetch(url),
        fetch(apiURL + '/layers.data/?where='+JSON.stringify(events_filters)+"&max_results=100")
    ]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        let dates = [];
        let new_cases = [];
        let active_cases_7 = [];
        let active_cases_14 = [];
        var spain_data = data[0]._items;
        var events_data = data[1]._items;

        for (var i = 0; i < spain_data.length; i++) {
            var doc = spain_data[i]
            dates.push(doc.date);
            new_cases.push(doc.new_cases);
            active_cases_7.push(doc.active_cases_7);
            active_cases_14.push(doc.active_cases_14);
            if (new Date(doc.date) >= new Date(max_date)) { break; }
        }

        // Define plot data (traces)
        var plt_data = [];
        for (var i=0; i<3; i++) {
            plt_data.push({
                x: dates,
                y: i==0? new_cases: i==1? active_cases_7: active_cases_14,
                mode: 'lines',
                type: 'scatter',
                hovertemplate: "%{x|%Y-%m-%d (%A)}<br>Cases: %{y}<extra></extra>",
                visible: i==0,
            });
        }

        // Define the plot layout
        layout = {
            plot_bgcolor:"white",
            paper_bgcolor:"#FFFFFF",
            template: {
                layout: {
                    hovermode: "closest",
                    paper_bgcolor: "white",
                    plot_bgcolor: "#E5ECF6",
                    xaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    yaxis: {automargin: true, gridcolor: "white", title: {standoff: 15}, zerolinecolor: "white"},
                    colorway: ["#636efa", "#EF553B", "#00cc96", "#ab63fa", "#FFA15A", "#19d3f3"],
                }
            },
            yaxis: {title: {text: "Daily cases"}},
            margin: {r: 55, t: 0, l: 0, b: 320},
            height: 600,
            updatemenus: [{
                y: 1,
                yanchor: 'top',
                buttons: [{
                    method: 'restyle',
                    args: ['visible', [true, false, false]],
                    label: i18next.t('plots.daily_incidence')
                }, {
                    method: 'restyle',
                    args: ['visible', [false, true, false]],
                    label: i18next.t('plots.cumulative_incidence_7_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [false, false, true]],
                    label: i18next.t('plots.cumulative_incidence_14_days')
                }],
                direction: "right",
                x: 0.01,
                xanchor: "left",
                yanchor: "top",
                legend: {traceorder: 'normal'},
            }],
            xaxis: {
                tickangle: 0,
                // tickvals: ['2020-03-15', '2020-06-20'],
                // ticktext : ['Lockdown starts - 2020-03-15', 'Lockdown ends - 2020-06-20'],
            },
            annotations: events_data.map(event=>{
                return {
                    x: event.d.date,
                    y: 0,
                    xref: 'x',
                    yref: 'paper',
                    text: event.d.name.slice(0, 25) + (event.d.name.length > 25 ? "..." : ""),
                    hovertext: "Click to go to the link.<br><br>"+event.d.date+": "+splitTextInLines(event.d.name, wordsPerLine=7, lineSep='<br>')+"<br><br>Description: "+splitTextInLines(event.d.description, wordsPerLine=7, lineSep='<br>'),
                    showarrow: true,
                    arrowhead: 0,
                    ax: 0,
                    ay: 40,
                    arrowcolor: '#636363',
                    textangle: -15,
                    xanchor: "right",
                    yanchor: "top",
                    captureevents: true,
                    customdata: event.d,
                }
            }),
            shapes: [
                ...events_data.map(event=>{
                    return {
                        type: 'line',
                        x0: event.d.date,
                        x1: event.d.date,
                        y0: 0,
                        y1: 1,
                        yref: 'paper',
                        line: {
                            color: 'grey',
                            width: 1.5,
                            dash: 'dot'
                        },
                    }
                }),
                {
                    type: 'rect',
                    xref: 'x',
                    yref: 'paper',
                    x0: '2020-01-01',
                    y0: 0,
                    x1: '2020-06-01',
                    y1: 1,
                    fillcolor: '#fcf951',
                    opacity: 0.2,
                    line: {
                        'width': 0,
                    }
                },
                {
                    type: 'rect',
                    xref: 'x',
                    yref: 'paper',
                    x0: '2020-06-01',
                    y0: 0,
                    x1: dates.map(e=>e).sort().reverse()[0],
                    y1: 1,
                    fillcolor: '#8abfff',
                    opacity: 0.2,
                    line: {
                        'width': 0,
                    }
                },
            ]
        };

        Plotly.plot('spainplot', plt_data, layout, ply_config).then(gd => {
            // gd.on('plotly_hoverannotation', (e) => {
            //     console.log(e)
            // });

            // gd.on('plotly_hover', (e) => {
            //     console.log(e)
            // });

            gd.on('plotly_clickannotation', (e) => {
                var win = window.open(e.annotation.customdata.link, '_blank');
                win.focus();
            });
        });
    }).catch(function (error) {
		// Hide div in case of fail
        $('#spainplot_div').hide()
        console.log('Spain plot not available.')
	});
}

function create_local_plot(province_id) {
    // LOCAL PLOT INCIDENCE 14 DAYS vs TIME
    let filters = {
        "ev": "ES.covid_cpro",
        "type": "covid19",
        "id": province_id,
    }
    let filters2 = {
        "type": "population",
        "date": '2020-03-01',
        "layer": "cnig_provincias",
        "id": province_id,
    }
    Promise.all([
		fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters2)+'&max_results=500'),
		fetch(apiURL+'/layers.data.consolidated/?where='+JSON.stringify(filters)
		                                                +'&max_results=500'
		                                                +'&projection={"date":1, "active_cases_14":1, "active_cases_7":1, "new_cases": 1}'),
        fetch(staticRoute+'data/cnig_provincias.geojson'),
	]).then(function (responses) {
        return Promise.all(responses.map(function (response) {
            return response.json();
        }));
    }).then(function (data) {
        // popData: 3400
        var popData = data[0]._items[0].population;

        var riskIndicator14days = null;
        var riskIndicator7days = null;
        var riskIndicatorDaily = null;
        for (var i = 0; i < 3; i++) {
            // covidData: [['new_normalility', ['01/03/2020', '02/03/2020'], [0, 3, 45, 125, ...]], ...]
            var covidData = [];

            if (i != 2) {
                var prev_current_state = null;
                data[1]._items.forEach(d => {
                    var incidence_attribute = ['active_cases_14', 'active_cases_7'][i]
                    var incidence = parseInt(100000 * d[incidence_attribute] / popData);
                    var current_state = risk_selector(incidence, incidence_attribute)

                    // Grouping the data while it's between the bounds defined on `selector` function ranges
                    if (current_state != prev_current_state) {
                        var obj = [current_state, [d.date], [incidence]];
                        covidData.push(obj);
                    } else {
                        covidData[covidData.length - 1][1].push(d.date);
                        covidData[covidData.length - 1][2].push(incidence);
                    }
                    prev_current_state = current_state;
                });
            } else {
                var dates = [];
                var cases = [];
                data[1]._items.forEach(d => {
                    dates.push(d.date);
                    cases.push(parseInt(d.new_cases));
                })
                covidData.push([risk_selector(null, 'new_cases'), dates, cases]);
            }

            if (i == 0) {
                riskIndicator14days = covidData;
            } else if (i == 1) {
                riskIndicator7days = covidData;
            } else {
                riskIndicatorDaily = covidData;
            }
        };

        var provincesNames = {};
        data[2].features.forEach(feat => provincesNames[feat.id] = feat.name);

        // Define plot data (traces)
        var plt_data = [];
        for (var i = 0; i < 3; i++) {
            var covidData = [riskIndicator14days, riskIndicator7days, riskIndicatorDaily][i]
            covidData.forEach(cd => {
                plt_data.push({
                    x: cd[1],
                    y: cd[2],
                    marker: {
                        color: cd[0],
                    },
                    mode: 'lines',
                    fill: 'tozeroy',
                    type: 'scatter',
                    hovertemplate: "%{x|%Y-%m-%d (%A)}<br>Incidence: %{y}<extra></extra>",
                    visible: i==0? true: false,
                })
            });
        }

        // Define the plot layout
        var cumulative_incidence_14_days = new Array(riskIndicator14days.length)
        var cumulative_incidence_7_days = new Array(riskIndicator7days.length)
        var daily_incidence = new Array(riskIndicatorDaily.length)
        layout = {
            yaxis: {title: {text: "Incidence"}},
            height: 500,
            margin: {r: 50, t: 0, l: 50, b: 50},
            showlegend: false,
            hovermode: 'closest',
            updatemenus: [{
                y: 1,
                yanchor: 'top',
                buttons: [{
                    method: 'restyle',
                    args: ['visible', [].concat(cumulative_incidence_14_days.fill(true),
                                                cumulative_incidence_7_days.fill(false),
                                                daily_incidence.fill(false))],
                    label: i18next.t('plots.cumulative_incidence_14_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(cumulative_incidence_14_days.fill(false),
                                                cumulative_incidence_7_days.fill(true),
                                                daily_incidence.fill(false))],
                    label: i18next.t('plots.cumulative_incidence_7_days')
                }, {
                    method: 'restyle',
                    args: ['visible', [].concat(cumulative_incidence_14_days.fill(false),
                                                cumulative_incidence_7_days.fill(false),
                                                daily_incidence.fill(true))],
                    label: i18next.t('plots.daily_incidence')
                }],
                direction: "right",
                x: 0.01,
                xanchor: "left",
                yanchor: "top",
                legend: {traceorder: 'normal'},
            }],
        };

        Plotly.newPlot('localplot', plt_data, layout, ply_config);

        // Change label of the div
        $('#selected_province')[0].innerHTML = provincesNames[province_id];
    }).catch(function (error) {
		// Hide div in case of fail
        $('#localplot_div').hide()
        console.log('Local plot not available.')
	});
}

function risk_selector(incidence, incidence_attribute) {
    if (incidence_attribute == 'active_cases_14') {
        if (incidence <= 25) {
            // New normality
            return '#D6ECC6';
        } else if (incidence <= 50) {
            // Low
            return '#ECE6C6';
        } else if (incidence <= 150) {
            // Medium
            return '#EEB63E';
        } else if (incidence <= 250) {
            // High
            return '#FF5C00';
        } else {
            // Very High
            return '#8B1700'
        }
    } else if (incidence_attribute == 'active_cases_7') {
        if (incidence <= 10) {
            // New normality
            return '#D6ECC6';
        } else if (incidence <= 25) {
            // Low
            return '#ECE6C6';
        } else if (incidence <= 75) {
            // Medium
            return '#EEB63E';
        } else if (incidence <= 125) {
            // High
            return '#FF5C00';
        } else {
            // Very High
            return '#8B1700'
        }
    } else {
        return '#000059'
    }
}

// =====================================  DATEPICKER  =====================================
function onChangeDate(new_date_str) {
    date_str = new_date_str
    $('#selected-date')[0].innerHTML = date_str


    if ($("ul#myTab a.active")[0].id == 'spain-tab') {
        // Recreate the global map and mark local map
        // to be recreated when opening the tab
        createGlobalMap();
        has_to_recreate_local_map = true;

    } else {
        // Recreate the local map, plot and table and
        // mark global map to be recreated when opening
        // the tab
        createLocalMap(selected_province);
        has_to_recreate_global_map = true;
    }
}
configCalendar(min_date, max_date, onChangeDate);


// =====================================  INITIALIZATION  =====================================
i18next.on('initialized', function(options) {
    createGlobalMap();

    create_spain_plot();
})