#!/bin/bash

set -x

# create and activate virtualenvironment
virtualenv env --python=python3
source env/bin/activate

# install requirements
pip3 install -r requirements.txt

##########
# NOTE: the following is only needed if pyflowmaps is not
# already installed and accessible in the PYTHONPATH
##########

# download flowmaps package
git clone https://gitlab.bsc.es/flowmaps/pyflowmaps.git

# install pyflowmaps requirements
pip3 install -r pyflowmaps/requirements.txt

# make a symbolic link to make the package accesible in the root dir (alternatively: add it to the PYTHONPATH)
ln -s pyflowmaps/flowmaps .
